k8s_interrogate:
  tags:
    - $K8S_TEST_CLUSTER_TAG
  variables:
    SKA_K8S_TOOLS_DEPLOY_IMAGE: artefact.skao.int/ska-cicd-k8s-tools-build-deploy:0.7.3
    KUBE_NAMESPACE: "ci-$CI_PROJECT_NAME-$CI_COMMIT_SHORT_SHA"
  image: $SKA_K8S_TOOLS_DEPLOY_IMAGE
  stage: test
  tags:
    - k8srunner
  when: manual
  before_script:
    - kubectl config get-contexts
    - kubectl auth can-i --list=true -n ci-$CI_PROJECT_NAME-$CI_COMMIT_SHORT_SHA
    - kubectl -n ci-$CI_PROJECT_NAME-$CI_COMMIT_SHORT_SHA get all
    - kubectl -n ci-$CI_PROJECT_NAME-$CI_COMMIT_SHORT_SHA get nodes
    - '[ -f .make/k8s.mk ] || (echo "File k8s.mk not included in Makefile; exit 1")'
    - 'make help | grep k8s-test'
  script:
    - make k8s-vars
    - kubectl -n $KUBE_NAMESPACE get all
    - kubectl -n $KUBE_NAMESPACE get pods
    - kubectl -n $KUBE_NAMESPACE describe pods
    - kubectl -n $KUBE_NAMESPACE logs -l app.kubernetes.io/name=ska-pst-dsp

test_k8s_bdd_storage:
  tags:
  - $K8S_TEST_CLUSTER_TAG
  variables:
    SKA_K8S_TOOLS_DEPLOY_IMAGE: artefact.skao.int/ska-cicd-k8s-tools-build-deploy:0.7.3
    PROJECT: ska-pst-dsp
    K8S_CHART_VALUES: test-storage.k8srunner.yaml
    K8S_CHART_VALUES_PATH: tests/integration
    K8S_CHART_PARAMS: 'test --values=tests/bdd/${K8S_CHART_VALUES}'
    K8S_CHART_PATH: charts/${PROJECT}
    K8S_FEATURES_PATH: tests/bdd/features
    KUBE_NAMESPACE: "ci-$CI_PROJECT_NAME-$CI_COMMIT_SHORT_SHA"
  dependencies:
    - k8s-test
  needs:
    - k8s-test
  image: $SKA_K8S_TOOLS_DEPLOY_IMAGE
  stage: test
  before_script:
    - apt update -y
    - apt install -y python3 python3-pip python3-venv
    - pip3 install --upgrade pip
    - make local-init-venv
    - mkdir -p build
  script:
    - kubectl create namespace $KUBE_NAMESPACE --dry-run=client -o yaml | kubectl apply -f -
    - kubectl auth can-i --list=true -n $KUBE_NAMESPACE
    - kubectl -n $KUBE_NAMESPACE get all
    - make local-test-bdd
    - kubectl delete ns $KUBE_NAMESPACE
  artifacts:
    name: "$KUBE_NAMESPACE"
    paths:
      - build/
    when: always
  rules:
  - exists:
    - tests/**/*
  when: manual

test_k8s_raw_correctness_tests:
    tags:
    - $K8S_TEST_CLUSTER_TAG
    variables:
      SKA_K8S_TOOLS_DEPLOY_IMAGE: artefact.skao.int/ska-cicd-k8s-tools-build-deploy:0.7.3
      KUBE_NAMESPACE: "ci-$CI_PROJECT_NAME-$CI_COMMIT_SHORT_SHA"
      K8S_CHART_PARAMS: "--values=tests/integration/k8s-test/correctness.bdd.yaml --set image.tag=`grep -m 1 -o '[0-9].*' .release`-dev.c$CI_COMMIT_SHORT_SHA"
      CI_K8S_WAIT: kubectl -n $KUBE_NAMESPACE wait --for=condition=complete --timeout=360s job -l bdd-test=correctness-validation
    image: $SKA_K8S_TOOLS_DEPLOY_IMAGE
    stage: test
    dependencies:
      - test_k8s_bdd_storage
    needs:
      - test_k8s_bdd_storage
    before_script:
      - kubectl config get-contexts
      - kubectl create namespace $KUBE_NAMESPACE --dry-run=client -o yaml | kubectl apply -f -
      - kubectl auth can-i --list=true -n $KUBE_NAMESPACE
      - kubectl -n $KUBE_NAMESPACE get pods
      - '[ -f .make/k8s.mk ] || (echo "File k8s.mk not included in Makefile; exit 1")'
      - 'make help | grep k8s-test'
      - make k8s-vars
      - make k8s-template-chart
      - make k8s-install-chart
      - kubectl -n $KUBE_NAMESPACE get all
      - make k8s-get-pods
      - echo "$CI_K8S_WAIT" && $CI_K8S_WAIT
    script:
      # - kubectl config get-contexts
      - mkdir -p build
      - kubectl auth can-i --list=true -n $KUBE_NAMESPACE
      - kubectl -n $KUBE_NAMESPACE get all
      - make k8s-vars
      - make k8s-get-pods
      - make k8s-pod-versions
      - make k8s-describe
      - make k8s-podlogs
      - make k8s-vars >> build/$KUBE_NAMESPACE.log
      - make k8s-get-pods >> build/$KUBE_NAMESPACE.log
      - make k8s-pod-versions >> build/$KUBE_NAMESPACE.log
      - make k8s-describe >> build/$KUBE_NAMESPACE.log
      - make k8s-podlogs >> build/$KUBE_NAMESPACE.log
      - cat build/$KUBE_NAMESPACE.log
      - mkdir -p build_k8s_test
      - mv build build_k8s_test/test_k8s_raw_correctness_tests
      - kubectl -n $KUBE_NAMESPACE delete pods,svc,daemonsets,deployments,replicasets,statefulsets,cronjobs,jobs,ingresses,configmaps --all
      - kubectl delete ns $KUBE_NAMESPACE
    artifacts:
      name: "$KUBE_NAMESPACE"
      paths:
        - build_k8s_test/
      when: always
    rules:
    - exists:
      - tests/**/*

test_k8s_raw_component_tests:
  tags:
  - $K8S_TEST_CLUSTER_TAG
  variables:
    SKA_K8S_TOOLS_DEPLOY_IMAGE: artefact.skao.int/ska-cicd-k8s-tools-build-deploy:0.7.3
    KUBE_NAMESPACE: "ci-$CI_PROJECT_NAME-$CI_COMMIT_SHORT_SHA"
    K8S_CHART_PARAMS: "--values=tests/integration/k8s-test/component.bdd.yaml --set image.tag=`grep -m 1 -o '[0-9].*' .release`-dev.c$CI_COMMIT_SHORT_SHA"
    CI_K8S_WAIT: kubectl -n $KUBE_NAMESPACE wait --for=condition=ready --timeout=360s pod -l bdd-test=component-sigint
  image: $SKA_K8S_TOOLS_DEPLOY_IMAGE
  stage: test
  dependencies:
    - test_k8s_raw_correctness_tests
  needs:
    - test_k8s_raw_correctness_tests
  before_script:
    - kubectl config get-contexts
    - kubectl create namespace $KUBE_NAMESPACE --dry-run=client -o yaml | kubectl apply -f -
    - kubectl auth can-i --list=true -n $KUBE_NAMESPACE
    - kubectl -n $KUBE_NAMESPACE get pods
    - '[ -f .make/k8s.mk ] || (echo "File k8s.mk not included in Makefile; exit 1")'
    - 'make help | grep k8s-test'
    - make k8s-vars
    - make k8s-template-chart
    - make k8s-install-chart
    - kubectl -n $KUBE_NAMESPACE get all
    - make k8s-get-pods
    - echo "$CI_K8S_WAIT" && $CI_K8S_WAIT
  script:
    # - kubectl config get-contexts
    - mkdir -p build
    - kubectl auth can-i --list=true -n $KUBE_NAMESPACE
    - kubectl -n $KUBE_NAMESPACE get all
    - make k8s-vars
    - make k8s-get-pods
    - make k8s-pod-versions
    - make k8s-describe
    - make k8s-podlogs
    - make k8s-vars >> build/$KUBE_NAMESPACE.log
    - make k8s-get-pods >> build/$KUBE_NAMESPACE.log
    - make k8s-pod-versions >> build/$KUBE_NAMESPACE.log
    - make k8s-describe >> build/$KUBE_NAMESPACE.log
    - make k8s-podlogs >> build/$KUBE_NAMESPACE.log
    - cat build/$KUBE_NAMESPACE.log
    - mkdir -p build_k8s_test
    - mv build build_k8s_test/test_k8s_raw_component_tests
    - kubectl -n $KUBE_NAMESPACE delete pods,svc,daemonsets,deployments,replicasets,statefulsets,cronjobs,jobs,ingresses,configmaps --all
    - kubectl delete ns $KUBE_NAMESPACE
  artifacts:
    name: "$KUBE_NAMESPACE"
    paths:
      - build_k8s_test/
    when: always
  rules:
  - exists:
    - tests/**/*

test_k8s_raw_performance_test:
  tags:
  - $K8S_TEST_CLUSTER_TAG
  variables:
    SKA_K8S_TOOLS_DEPLOY_IMAGE: artefact.skao.int/ska-cicd-k8s-tools-build-deploy:0.7.3
    KUBE_NAMESPACE: "ci-$CI_PROJECT_NAME-$CI_COMMIT_SHORT_SHA"
    K8S_CHART_PARAMS: "--values=tests/integration/k8s-test/performance.bdd.yaml --set image.tag=`grep -m 1 -o '[0-9].*' .release`-dev.c$CI_COMMIT_SHORT_SHA"
    CI_K8S_WAIT: kubectl -n $KUBE_NAMESPACE wait --for=condition=complete --timeout=360s job -l bdd-test=performance
  image: $SKA_K8S_TOOLS_DEPLOY_IMAGE
  stage: test
  dependencies:
    - test_k8s_raw_component_tests
  needs:
    - test_k8s_raw_component_tests
  before_script:
    - kubectl config get-contexts
    - kubectl create namespace $KUBE_NAMESPACE --dry-run=client -o yaml | kubectl apply -f -
    - kubectl auth can-i --list=true -n $KUBE_NAMESPACE
    - kubectl -n $KUBE_NAMESPACE get pods
    - '[ -f .make/k8s.mk ] || (echo "File k8s.mk not included in Makefile; exit 1")'
    - 'make help | grep k8s-test'
    - make k8s-vars
    - make k8s-template-chart
    - make k8s-install-chart
    - kubectl -n $KUBE_NAMESPACE get all
    - make k8s-get-pods
    - echo "$CI_K8S_WAIT" && $CI_K8S_WAIT
  script:
    # - kubectl config get-contexts
    - mkdir -p build
    - kubectl auth can-i --list=true -n $KUBE_NAMESPACE
    - kubectl -n $KUBE_NAMESPACE get all
    - make k8s-vars
    - make k8s-get-pods
    - make k8s-pod-versions
    - make k8s-describe
    - make k8s-podlogs
    - make k8s-vars >> build/$KUBE_NAMESPACE.log
    - make k8s-get-pods >> build/$KUBE_NAMESPACE.log
    - make k8s-pod-versions >> build/$KUBE_NAMESPACE.log
    - make k8s-describe >> build/$KUBE_NAMESPACE.log
    - make k8s-podlogs >> build/$KUBE_NAMESPACE.log
    - cat build/$KUBE_NAMESPACE.log
    - mkdir -p build_k8s_test
    - mv build build_k8s_test/test_k8s_raw_performance_test
    - kubectl -n $KUBE_NAMESPACE delete pods,svc,daemonsets,deployments,replicasets,statefulsets,cronjobs,jobs,ingresses,configmaps --all
    - kubectl delete ns $KUBE_NAMESPACE
  artifacts:
    name: "$KUBE_NAMESPACE"
    paths:
      - build_k8s_test/
    when: always
  rules:
  - exists:
    - tests/**/*
