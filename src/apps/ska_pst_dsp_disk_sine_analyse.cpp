/*
 * Copyright 2023 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <unistd.h>
#include <iostream>
#include <cfloat>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <spdlog/spdlog.h>

#include "ska/pst/common/utils/Logging.h"
#include "ska/pst/common/utils/DataUnpacker.h"
#include "ska/pst/common/utils/FileReader.h"
#include "ska/pst/dsp/definitions.h"

void usage();
void write_to_file(const std::string& output_filename, const ska::pst::common::AsciiHeader& header, const std::vector<std::vector<float>>& bandpass);

auto main(int argc, char *argv[]) -> int
{
  ska::pst::common::setup_spdlog();
  char verbose = 0;

  opterr = 0;

  int c = 0;

  std::string output_file;

  while ((c = getopt(argc, argv, "ho:v")) != EOF)
  {
    switch(c)
    {
      case 'h':
        usage();
        exit(EXIT_SUCCESS);
        break;

      case 'o':
        output_file = std::string(optarg);
        break;

      case 'v':
        verbose++;
        break;

      default:
        std::cerr << "ERROR: unrecognised option: -" << char(optopt) << std::endl;
        usage();
        return EXIT_FAILURE;
        break;
    }
  }

  if (verbose > 0)
  {
    spdlog::set_level(spdlog::level::debug);
    if (verbose > 1)
    {
      spdlog::set_level(spdlog::level::trace);
    }
  }

  // Check arguments
  if ((argc - optind) != 2)
  {
    SPDLOG_ERROR("ERROR: 2 command line arguments are expected");
    usage();
    return EXIT_FAILURE;
  }

  std::string data_file(argv[optind]); // NOLINT
  std::string weights_file(argv[optind+1]); // NOLINT
  int return_code = 0;

  try
  {
    ska::pst::common::FileReader data_file_reader(data_file);
    ska::pst::common::FileReader weights_file_reader(weights_file);
    ssize_t data_bytes_remaining = data_file_reader.get_file_size();
    ssize_t weights_bytes_remaining = weights_file_reader.get_file_size();

    ssize_t data_bytes_read = data_file_reader.read_header();
    ssize_t weights_bytes_read = weights_file_reader.read_header();
    data_bytes_remaining -= data_bytes_read;
    weights_bytes_remaining -= weights_bytes_read;

    const ska::pst::common::AsciiHeader& data_header = data_file_reader.get_header();
    const ska::pst::common::AsciiHeader& weights_header = weights_file_reader.get_header();

    SPDLOG_DEBUG("ska_pst_dsp_disk_sine_analyse configuring data unpacker");
    ska::pst::common::DataUnpacker data_unpacker;
    data_unpacker.configure(data_header, weights_header);

    auto data_bufsz = ssize_t(data_header.get_uint32("RESOLUTION"));
    auto weights_bufsz = ssize_t(weights_header.get_uint32("RESOLUTION"));
    SPDLOG_DEBUG("ska_pst_dsp_disk_sine_analyse RESOLUTION data={} weights={}", data_bufsz, weights_bufsz);

    static constexpr uint32_t process_block_factor = 64;
    data_bufsz *= process_block_factor;
    weights_bufsz *= process_block_factor;

    std::vector<char> data_buffer(data_bufsz);
    std::vector<char> weights_buffer(weights_bufsz);

    data_bytes_read = 0;
    weights_bytes_read = 0;
    bool data_valid = true;

    while (data_valid && data_bytes_remaining > 0 && weights_bytes_remaining > 0)
    {
      ssize_t data_bytes_to_read = std::min(data_bytes_remaining, data_bufsz);
      ssize_t weights_bytes_to_read = std::min(weights_bytes_remaining, weights_bufsz);

      SPDLOG_DEBUG("Reading {} bytes, remaining={}", data_bytes_to_read, data_bytes_remaining);

      data_file_reader.read_data(&data_buffer[0], data_bytes_to_read);
      weights_file_reader.read_data(&weights_buffer[0], weights_bytes_to_read);

      data_unpacker.integrate_bandpass(&data_buffer[0], data_bytes_to_read, &weights_buffer[0], weights_bytes_to_read);

      SPDLOG_DEBUG("Integrating done!");

      data_bytes_read += data_bytes_to_read;
      weights_bytes_read += weights_bytes_to_read;

      data_bytes_remaining -= data_bytes_to_read;
      weights_bytes_remaining -= weights_bytes_to_read;
    }

    data_file_reader.close_file();
    weights_file_reader.close_file();
    std::vector<std::vector<float>>& bandpass = data_unpacker.get_bandpass();

    unsigned nchan = bandpass.size();
    unsigned max_channel{0}, max_pol{0};
    float max_bandpass = -FLT_MAX;
    float max_freq = -FLT_MAX;
    float bw = data_header.get_float("BW");
    float freq = data_header.get_float("FREQ");
    float channel_bw = bw / float(nchan);
    float first_channel_cfreq = (freq - (bw/2)) + (channel_bw/2);

    for (unsigned ichan=0; ichan<nchan; ichan++)
    {
      for (unsigned ipol=0; ipol<bandpass[ichan].size(); ipol++)
      {
        if (bandpass[ichan][ipol] > max_bandpass)
        {
          max_bandpass = bandpass[ichan][ipol];
          max_channel = ichan;
          max_pol = ipol;
          max_freq = first_channel_cfreq + (float(ichan) * channel_bw);
        }
      }
    }
    spdlog::info("Bandpass maximum value={} found at frequency={} MHz in channel={} pol={}", max_bandpass, max_freq, max_channel, max_pol);

    if (output_file.length() > 0)
    {
      write_to_file(output_file, data_header, bandpass);
    }
  }
  catch (std::exception& exc)
  {
    SPDLOG_ERROR("Exception caught: {}", exc.what());
    return_code = 1;
  }

  SPDLOG_DEBUG("return return_code={}", return_code);
  return return_code;
}

void write_to_file(const std::string& output_filename, const ska::pst::common::AsciiHeader& header, const std::vector<std::vector<float>>& bandpass)
{
  int flags = O_WRONLY | O_CREAT | O_TRUNC;
  int perms = S_IWUSR | S_IRUSR | S_IRGRP | S_IROTH;

  SPDLOG_DEBUG("ska::pst::dsp::write_to_file opening {}", output_filename);
  int fd = open(output_filename.c_str(), flags, perms); // NOLINT
  if (fd < 0)
  {
    SPDLOG_ERROR("ska::pst::dsp::write_to_file failed to open {} for writing.", output_filename);
    throw std::runtime_error("Unable to open file " + output_filename);
  }

  uint32_t nchan = bandpass.size();
  uint32_t npol = bandpass[1].size();
  float freq = header.get_float("FREQ");
  float bw = header.get_float("BW");

  SPDLOG_DEBUG("ska::pst::dsp::write_to_file nchan={} npol={} freq={} bw={}", nchan, npol, freq, bw);
  ssize_t wrote{0};

  wrote = write(fd, reinterpret_cast<void *>(&nchan), sizeof(uint32_t));
  if (wrote != sizeof(uint32_t))
  {
    SPDLOG_ERROR("ska::pst::dsp::write_to_file failed to nchan to file, write returned {}", wrote);
    throw std::runtime_error("failed to write nchan to file");
  }
  wrote = write(fd, reinterpret_cast<void *>(&npol), sizeof(uint32_t));
  if (wrote != sizeof(uint32_t))
  {
    SPDLOG_ERROR("ska::pst::dsp::write_to_file failed to npol to file, write returned {}", wrote);
    throw std::runtime_error("failed to write mpol to file");
  }

  std::vector<float> channels(nchan);

  float channel_bw = bw / float(nchan);
  float first_channel_cfreq = (freq - (bw/2)) + (channel_bw/2);
  for (unsigned ichan=0; ichan<nchan; ichan++)
  {
    channels[ichan] = first_channel_cfreq + (float(ichan) * channel_bw);
    SPDLOG_TRACE("channels[{}]={}", ichan, channels[ichan]);
  }

  // write the x-axis bandpass centre frequencies to the file
  ssize_t bytes_to_write = nchan * sizeof(float);
  wrote = write(fd, reinterpret_cast<void *>(&channels[0]), bytes_to_write);
  if (wrote != bytes_to_write)
  {
    SPDLOG_ERROR("ska::pst::dsp::write_to_file failed to write {} bytes to {}", bytes_to_write, bytes_to_write);
    throw std::runtime_error("failed to write to output_file");
  }

  for (unsigned ipol=0; ipol<npol; ipol++)
  {
    for (unsigned ichan=0; ichan<nchan; ichan++)
    {
      channels[ichan] = bandpass[ichan][ipol];
    }
    wrote = write(fd, reinterpret_cast<void *>(&channels[0]), bytes_to_write);
    if (wrote != bytes_to_write)
    {
      SPDLOG_ERROR("ska::pst::dsp::write_to_file failed to write {} bytes to {}", bytes_to_write, bytes_to_write);
      throw std::runtime_error("failed to write to output_file");
    }
  }

  if (::close(fd) < 0)
  {
    SPDLOG_ERROR("ska::pst::dsp::write_to_file ::close({}) failed: {}", fd, strerror(errno));
  }
}

void usage()
{
  std::cout << "Usage: ska_pst_dsp_disk_sine_analyse [options] data_file weights_file" << std::endl;
  std::cout << std::endl;
  std::cout << "  data_file     raw data file that will be validated against the pseudo-random sequence" << std::endl;
  std::cout << "  weights_file  weights file containing scale and weights corresponding to the data file" << std::endl;
  std::cout << "  -o output     write bandpass to output file raw/binary FP ordered file" << std::endl;
  std::cout << "  -h            print this help text" << std::endl;
  std::cout << "  -v            verbose output" << std::endl;
}
