/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <unistd.h>
#include <csignal>
#include <iostream>
#include <spdlog/spdlog.h>

#include "ska/pst/common/utils/Logging.h"
#include "ska/pst/dsp/definitions.h"
#include "ska/pst/dsp/disk/DiskMonitor.h"

void usage();
void signal_handler(int signal_value);
int quit_threads = 0;

auto main(int argc, char *argv[]) -> int
{
  ska::pst::common::setup_spdlog();
  std::string recording_path = "/tmp";

  int timeout = 0;

  char verbose = 0;

  opterr = 0;

  int c = 0;

  while ((c = getopt(argc, argv, "d:hsv")) != EOF)
  {
    switch(c)
    {
      case 'd':
        recording_path = std::string(optarg);
        break;

      case 'h':
        usage();
        exit(EXIT_SUCCESS);
        break;

      case 'v':
        verbose++;
        break;

      default:
        std::cerr << "ERROR: unrecognised option: -" << char(optopt) << std::endl;
        usage();
        return EXIT_FAILURE;
        break;
    }
  }

  if (verbose > 0)
  {
    spdlog::set_level(spdlog::level::debug);
    if (verbose > 1)
    {
      spdlog::set_level(spdlog::level::trace);
    }
  }

  // Check arguments
  if ((argc - optind) != 1)
  {
    SPDLOG_ERROR("ERROR: 1 command line argument expected");
    usage();
    return EXIT_FAILURE;
  }

  signal(SIGINT, signal_handler);
  signal(SIGTERM, signal_handler);

  std::string config_file(argv[optind]); // NOLINT
  ska::pst::dsp::DiskMonitor diskmon(recording_path);
  int return_code = 0;

  try
  {
    // prepare the config and header
    ska::pst::common::AsciiHeader config;

    // config file for this data stream
    SPDLOG_DEBUG("loading configuration from {}", config_file);
    config.load_from_file(config_file);

    diskmon.configure_beam(config);
    diskmon.configure_scan(config);

    bool persist = true;
    uint64_t deciseconds = 0;
    while (persist)
    {
      usleep(ska::pst::dsp::microseconds_per_decisecond);
      if (deciseconds % ska::pst::dsp::deciseconds_per_second == 0)
      {
        size_t available = diskmon.get_disk_available();
        size_t capacity = diskmon.get_disk_capacity();
        double percent_free = ska::pst::dsp::centiles * double(available) / double(capacity);
        SPDLOG_INFO("DiskMonitor filesystem={} space={} capacity={} percent_free={}", recording_path, available, capacity, percent_free);
      }
      deciseconds++;
      persist &= !quit_threads;
    }

    diskmon.deconfigure_scan();
    diskmon.deconfigure_beam();
  }
  catch (std::exception& exc)
  {
    SPDLOG_ERROR("Exception caught: {}", exc.what());
    return_code = 1;
  }

  SPDLOG_DEBUG("return return_code={}", return_code);
  return return_code;
}

void usage()
{
  std::cout << "Usage: ska_pst_dsp_disk_monitor [options] config" << std::endl;
  std::cout << "Monitor the file system specified in the configuration file ";
  std::cout << "and report the disk available statistics. Quit with CTRL+C." << std::endl;
  std::cout << std::endl;
  std::cout << "  config      ascii file containing observation configuration" << std::endl;
  std::cout << "  -d path     recording base path to monitor [default /tmp]" << std::endl;
  std::cout << "  -h          print this help text" << std::endl;
  std::cout << "  -v          verbose output" << std::endl;
}

void signal_handler(int signalValue)
{
  SPDLOG_INFO("received signal {}", signalValue);
  if (quit_threads)
  {
    SPDLOG_WARN("received signal {} twice, exiting", signalValue);
    exit(EXIT_FAILURE);
  }
  quit_threads = 1;
}