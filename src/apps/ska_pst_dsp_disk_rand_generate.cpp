/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <unistd.h>
#include <iostream>
#include <spdlog/spdlog.h>

#include "ska/pst/common/utils/Logging.h"
#include "ska/pst/common/utils/RandomSequence.h"
#include "ska/pst/common/utils/Time.h"
#include "ska/pst/smrb/DataBlockWrite.h"

#include "ska/pst/dsp/definitions.h"

static constexpr double default_scan_len_max = 15;

void usage();

auto main(int argc, char *argv[]) -> int
{
  ska::pst::common::setup_spdlog();
  int timeout = 0;

  double scan_len_max = default_scan_len_max;

  char verbose = 0;

  opterr = 0;

  int c = 0;

  while ((c = getopt(argc, argv, "hl:v")) != EOF)
  {
    switch(c)
    {
      case 'h':
        usage();
        exit(EXIT_SUCCESS);
        break;

      case 'l':
        scan_len_max = atof(optarg);
        break;

      case 'v':
        verbose++;
        break;

      default:
        std::cerr << "ERROR: unrecognised option: -" << char(optopt) << std::endl;
        usage();
        return EXIT_FAILURE;
        break;
    }
  }

  if (verbose > 0)
  {
    spdlog::set_level(spdlog::level::debug);
    if (verbose > 1)
    {
      spdlog::set_level(spdlog::level::trace);
    }
  }

  // Check arguments
  if ((argc - optind) != 1)
  {
    SPDLOG_ERROR("ERROR: 1 command line argument expected");
    usage();
    return EXIT_FAILURE;
  }

  std::string config_file(argv[optind]); // NOLINT
  int return_code = 0;

  try
  {
    // config file for this data stream
    SPDLOG_DEBUG("loading configuration from {}", config_file);
    ska::pst::common::AsciiHeader config;
    config.load_from_file(config_file);

    std::string data_key = config.get_val("DATA_KEY");
    std::string weights_key = config.get_val("WEIGHTS_KEY");

    ska::pst::smrb::DataBlockWrite db(data_key);
    db.connect(0);
    db.lock();

    ska::pst::smrb::DataBlockWrite wb(weights_key);
    wb.connect(0);
    wb.lock();

    // determine the expected data rate
    uint32_t nchan = config.get_uint32("NCHAN");
    uint32_t npol = config.get_uint32("NPOL");
    uint32_t ndim = config.get_uint32("NDIM");
    uint32_t nbit = config.get_uint32("NBIT");
    uint32_t nsamp_pp = config.get_uint32("NSAMP_PP");

    uint64_t data_bufsz = db.get_data_bufsz();
    uint64_t weights_bufsz = wb.get_data_bufsz();
    uint32_t data_resolution = uint32_t(nchan * npol * ndim * nbit * nsamp_pp) / ska::pst::dsp::bits_per_byte;
    uint32_t weights_resolution = uint32_t(nchan * ska::pst::dsp::weights_nbit) / ska::pst::dsp::bits_per_byte;
    double data_weights_factor = double(data_resolution) / double(weights_resolution);

    if (data_bufsz % data_resolution != 0)
    {
      throw std::runtime_error("data_bufsz is not a multiple of data_resolution");
    }
    if (weights_bufsz % weights_resolution != 0)
    {
      throw std::runtime_error("weights_bufsz is not a multiple of weights_resolution");
    }

    double tsamp = config.get_double("TSAMP");
    double tsamp_seconds = tsamp / ska::pst::dsp::microseconds_per_second;
    double data_bytes_per_second = double(nchan * npol * ndim * nbit / ska::pst::dsp::bits_per_byte) / tsamp_seconds;
    double weights_bytes_per_second = data_bytes_per_second / data_weights_factor;

    ska::pst::common::AsciiHeader data_scan_config;
    ska::pst::common::AsciiHeader weights_scan_config;
    ska::pst::common::AsciiHeader data_header;
    ska::pst::common::AsciiHeader weights_header;
    ska::pst::common::Time now(time(nullptr));

    data_scan_config.clone(config);
    data_scan_config.set("BYTES_PER_SECOND", data_bytes_per_second);
    data_scan_config.set("RESOLUTION", data_resolution);

    weights_scan_config.clone(config);
    weights_scan_config.set("BYTES_PER_SECOND", weights_bytes_per_second);
    weights_scan_config.set("RESOLUTION", weights_resolution);

    db.write_config(data_scan_config.raw());
    wb.write_config(weights_scan_config.raw());

    data_header.clone(data_scan_config);
    data_header.set_val("UTC_START", now.get_gmtime());
    data_header.set_val("OBS_OFFSET", "0");
    data_header.set_val("SCAN_ID", "12345");

    weights_header.clone(weights_scan_config);
    weights_header.set_val("UTC_START", now.get_gmtime());
    weights_header.set_val("OBS_OFFSET", "0");
    weights_header.set_val("SCAN_ID", "12345");

    db.write_header(data_header.raw());
    wb.write_header(weights_header.raw());

    ska::pst::common::RandomSequence ds;
    ska::pst::common::RandomSequence ws;

    ds.configure(data_header);
    ws.configure(weights_header);

    db.open();
    wb.open();

    double bytes_to_write = uint64_t(floor(data_bytes_per_second * scan_len_max));
    uint64_t blocks_to_write = bytes_to_write / data_bufsz;
    SPDLOG_DEBUG("main: data_bytes_per_second={} scan_len_max={} bytes_to_write={} blocks_to_write={}", data_bytes_per_second, scan_len_max, bytes_to_write, blocks_to_write);
    for (uint64_t i=0; i<blocks_to_write; i++)
    {
      char * buffer = db.open_block();
      ds.generate(reinterpret_cast<uint8_t*>(buffer), data_bufsz);
      db.close_block(db.get_data_bufsz());

      buffer = wb.open_block();
      ws.generate(reinterpret_cast<uint8_t*>(buffer), weights_bufsz);
      wb.close_block(wb.get_data_bufsz());
    }
    db.close();
    wb.close();

    db.unlock();
    db.disconnect();
    wb.unlock();
    wb.disconnect();
  }
  catch (std::exception& exc)
  {
    SPDLOG_ERROR("Exception caught: {}", exc.what());
    return_code = 1;
  }

  SPDLOG_DEBUG("return return_code={}", return_code);
  return return_code;
}

void usage()
{
  std::cout << "Usage: ska_pst_dsp_disk_rand_generate [options] config" << std::endl;
  std::cout << std::endl;
  std::cout << "  config      ascii file containing observation configuration" << std::endl;
  std::cout << "  -h          print this help text" << std::endl;
  std::cout << "  -l nsecs    generate data stream for nsecs [default " << default_scan_len_max << "]" << std::endl;
  std::cout << "  -v          verbose output" << std::endl;
}