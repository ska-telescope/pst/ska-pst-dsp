/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <unistd.h>
#include <csignal>
#include <iostream>
#include <vector>
#include <spdlog/spdlog.h>
#include <thread>

#include "ska/pst/dsp/config.h"
#include "ska/pst/dsp/definitions.h"
#include "ska/pst/dsp/disk/DiskManager.h"
#include "ska/pst/common/utils/AsciiHeader.h"
#include "ska/pst/common/utils/Logging.h"

#ifdef HAVE_PROTOBUF_gRPC_SKAPSTLMC
#include "ska/pst/common/lmc/LmcService.h"
#include "ska/pst/dsp/disk/lmc/DspDiskLmcServiceHandler.h"
#endif

void usage();
void signal_handler(int signal_value);
bool signal_received = false;

auto main(int argc, char *argv[]) -> int
{
  ska::pst::common::setup_spdlog();
  static constexpr int test_duration_ms = 100;

  std::string config_file{};

  std::string recording_path = "/tmp";

  bool use_o_direct = false;

#ifdef HAVE_PROTOBUF_gRPC_SKAPSTLMC
  std::string service_name = "DSP.DISK";
  int control_port = -1;
#endif

  int verbose = 0;

  opterr = 0;

  int c = 0;
#ifdef HAVE_PROTOBUF_gRPC_SKAPSTLMC
  while ((c = getopt(argc, argv, "c:d:f:hov")) != EOF)
#else
  while ((c = getopt(argc, argv, "d:f:hov")) != EOF)
#endif
  {
    switch(c)
    {
#ifdef HAVE_PROTOBUF_gRPC_SKAPSTLMC
      case 'c':
        control_port = atoi(optarg);
        break;
#endif
      case 'd':
        recording_path = std::string(optarg);
        break;

      case 'f':
        config_file = std::string(optarg);
        break;

      case 'h':
        usage();
        exit(EXIT_SUCCESS);
        break;

      case 'o':
        use_o_direct = true;
        break;

      case 'v':
        verbose++;
        break;

      default:
        std::cerr << "Unrecognised option [" << c << "]" << std::endl;
        usage();
        return EXIT_FAILURE;
        break;
    }
  }

  if (verbose > 0)
  {
    spdlog::set_level(spdlog::level::debug);
    if (verbose > 1)
    {
      spdlog::set_level(spdlog::level::trace);
    }
  }

  SPDLOG_DEBUG("config_file={}", config_file);
#ifdef HAVE_PROTOBUF_gRPC_SKAPSTLMC
  SPDLOG_DEBUG("control_port={}", control_port);
#endif
  // Check arguments
  int nargs = argc - optind;
  if (nargs != 0)
  {
    SPDLOG_ERROR("ERROR: 0 command line arguments expected");
    usage();
    return EXIT_FAILURE;
  }

#ifdef HAVE_PROTOBUF_gRPC_SKAPSTLMC
  if (config_file.length() == 0 && control_port == -1)
  {
    SPDLOG_ERROR("ERROR: require either a configuration file or control port");
#else
  if (config_file.length() == 0)
  {
    SPDLOG_ERROR("ERROR: require a configuration file");
#endif
    usage();
    return EXIT_FAILURE;
  }

  signal(SIGINT, signal_handler);
  signal(SIGTERM, signal_handler);

  try
  {
    std::shared_ptr<ska::pst::dsp::DiskManager> dsp_disk = std::make_shared<ska::pst::dsp::DiskManager>(recording_path, use_o_direct);

#ifdef HAVE_PROTOBUF_gRPC_SKAPSTLMC
    std::unique_ptr<ska::pst::common::LmcService> lmc_service;
    if (control_port != -1)
    {
      SPDLOG_INFO("Setting up gRPC LMC service on port {}", control_port);
      auto lmc_handler = std::make_shared<ska::pst::dsp::DspDiskLmcServiceHandler>(dsp_disk);
      lmc_service = std::make_unique<ska::pst::common::LmcService>(service_name, lmc_handler, control_port);
      lmc_service->start();
    }
    else
#endif
    {
      // performs configure_beam, configure_scan and start_scan using config_file
      dsp_disk->configure_from_file(config_file);

      dsp_disk->stop_scan();
      dsp_disk->deconfigure_scan();
      dsp_disk->deconfigure_beam();
    }

#ifdef HAVE_PROTOBUF_gRPC_SKAPSTLMC
    if (lmc_service)
    {
      while (!signal_received)
      {
        usleep(ska::pst::dsp::microseconds_per_decisecond);
      }

      // signal the gRPC server to exit
      SPDLOG_INFO("Stopping gRPC LMC service");
      lmc_service->stop();
      SPDLOG_TRACE("gRPC LMC service has stopped");
    }
#endif

    dsp_disk->quit();
  }
  catch (std::exception& exc)
  {
    SPDLOG_ERROR("Exception caught: {}", exc.what());
    return EXIT_FAILURE;
  }

  return 0;
}

void usage()
{
  std::cout << "Usage: ska_pst_dsp_disk [options]" << std::endl;
  std::cout <<   "Create DSP Disk Recorder from the configuration file or via control command." << std::endl;
  std::cout <<   "Quit with CTRL+C." << std::endl;
  std::cout << std::endl;
#ifdef HAVE_PROTOBUF_gRPC_SKAPSTLMC
  std::cout << "  -c port     port on which to accept control commands" << std::endl;
#endif
  std::cout << "  -d path     write output files to the recording path [default /tmp]" << std::endl;
  std::cout << "  -f config   ascii file containing observation configuration" << std::endl;
  std::cout << "  -h          print this help text" << std::endl;
  std::cout << "  -v          verbose output" << std::endl;
  std::cout << "  -o          use O_DIRECT for I/O transactions" << std::endl;
}

void signal_handler(int signalValue)
{
  SPDLOG_INFO("received signal {}", signalValue);
  if (signal_received)
  {
    SPDLOG_WARN("received signal {} twice, exiting", signalValue);
    exit(EXIT_FAILURE);
  }
  signal_received = true;
}
