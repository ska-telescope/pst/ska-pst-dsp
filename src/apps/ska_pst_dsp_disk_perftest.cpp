/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <unistd.h>
#include <csignal>
#include <iostream>
#include <spdlog/spdlog.h>

#include "ska/pst/smrb/DataBlockCreate.h"
#include "ska/pst/smrb/DataBlockWrite.h"
#include "ska/pst/dsp/definitions.h"
#include "ska/pst/dsp/disk/DiskMonitor.h"
#include "ska/pst/dsp/disk/StreamWriter.h"

void usage();
void signal_handler(int signal_value);
int quit_threads = 0;

static constexpr double default_scan_len_max = 30;

auto main(int argc, char *argv[]) -> int
{
  int timeout = 0;

  char verbose = 0;

  bool use_o_direct = false;

  bool lock_data_block_memory = true;

  double scan_len_max = default_scan_len_max;

  std::string recording_path = "/tmp";

  opterr = 0;

  int c = 0;

  while ((c = getopt(argc, argv, "d:hl:mov")) != EOF)
  {
    switch(c)
    {
      case 'd':
        recording_path = std::string(optarg);
        break;

      case 'h':
        usage();
        exit(EXIT_SUCCESS);
        break;

      case 'v':
        verbose++;
        break;

      case 'l':
        scan_len_max = atof(optarg);
        break;

      case 'm':
        lock_data_block_memory = false;
        break;

      case 'o':
        use_o_direct = true;
        break;

      default:
        std::cerr << "ERROR: unrecognised option: -" << char(optopt) << std::endl;
        usage();
        return EXIT_FAILURE;
        break;
    }
  }

  if (verbose > 0)
  {
    spdlog::set_level(spdlog::level::debug);
    if (verbose > 1)
    {
      spdlog::set_level(spdlog::level::trace);
    }
  }

  // Check arguments
  if ((argc - optind) != 1)
  {
    SPDLOG_ERROR("ERROR: 1 command line argument expected");
    usage();
    return EXIT_FAILURE;
  }

  signal(SIGINT, signal_handler);
  signal(SIGTERM, signal_handler);

  std::string config_file(argv[optind]); // NOLINT
  ska::pst::dsp::DiskMonitor disk_monitor(recording_path);
  ska::pst::dsp::StreamWriter stream_writer(disk_monitor, use_o_direct);
  int return_code = 0;

  try
  {
    // config file for this data stream
    SPDLOG_DEBUG("loading configuration from {}", config_file);
    ska::pst::common::AsciiHeader config;
    config.load_from_file(config_file);

    ska::pst::smrb::DataBlockCreate dbc("dada");
    static constexpr uint64_t header_nbufs = 8;
    static constexpr uint64_t header_bufsz = 4096;
    static constexpr uint64_t data_nbufs = 8;
    static constexpr uint64_t data_bufsz = 33554432;
    static constexpr unsigned nreaders = 1;
    static constexpr int device = -1;
    dbc.create(header_nbufs, header_bufsz, data_nbufs, data_bufsz, nreaders, device);
    if (lock_data_block_memory == true)
    {
      dbc.lock_memory();
    }
    dbc.page();

    ska::pst::smrb::DataBlockWrite dbw("dada");
    dbw.connect(0);
    dbw.lock();

    // determine the expected data rate from the configuration file
    uint32_t nchan = config.get_uint32("NCHAN");
    uint32_t npol = config.get_uint32("NPOL");
    uint32_t ndim = config.get_uint32("NDIM");
    uint32_t nbit = config.get_uint32("NBIT");
    uint32_t nsamp_pp = config.get_uint32("NSAMP_PP");

    double resolution = double(nchan * npol * ndim * nbit * nsamp_pp) / ska::pst::dsp::bits_per_byte;
    double tsamp = config.get_double("TSAMP");
    double tsamp_seconds = tsamp / ska::pst::dsp::microseconds_per_second;

    double bytes_per_second = double(nchan * npol * ndim * nbit / ska::pst::dsp::bits_per_byte) / tsamp_seconds;

    // prepare the headers for the data block
    ska::pst::common::AsciiHeader db_config, db_header;
    db_config.set("BYTES_PER_SECOND", bytes_per_second);
    db_config.set("RESOLUTION", resolution);
    dbw.write_config(db_config.raw());

    db_header.clone(db_config);
    db_header.set_val("UTC_START", "1970-01-01-00:00:00");
    db_header.set_val("OBS_OFFSET", "0");
    db_header.set_val("SCAN_ID", "12345");
    dbw.write_header(db_header.raw());

    // prepare the beam configuration
    ska::pst::common::AsciiHeader beam_config, scan_config, start_scan_config;
    beam_config.set_val("KEY", "dada");
    beam_config.set_val("RECORDING_BASE_PATH", recording_path);
    beam_config.set_val("RECORDING_SUFFIX", "data");
    disk_monitor.configure_beam(beam_config);
    stream_writer.configure_beam(beam_config);

    // prepare the scan configuration
    scan_config.set("BYTES_PER_SECOND", bytes_per_second);
    scan_config.set("SCANLEN_MAX", scan_len_max);
    disk_monitor.configure_scan(scan_config);
    stream_writer.configure_scan(scan_config);

    // prepare the start scan configuration
    SPDLOG_DEBUG("main: disk_monitor.start_scan()");
    disk_monitor.start_scan(start_scan_config);
    stream_writer.start_scan(start_scan_config);

    dbw.open();
    double bytes_to_write = uint64_t(floor(bytes_per_second * scan_len_max));
    uint64_t blocks_to_write = bytes_to_write / dbw.get_data_bufsz();
    SPDLOG_DEBUG("main: bytes_per_second={} scan_len_max={} bytes_to_write={} blocks_to_write={}", bytes_per_second, scan_len_max, bytes_to_write, blocks_to_write);
    for (uint64_t i=0; i<blocks_to_write; i++)
    {
      dbw.open_block();
      dbw.close_block(dbw.get_data_bufsz());
    }
    dbw.close();

    SPDLOG_DEBUG("main: stream_writer.stop_scan()");
    stream_writer.stop_scan();
    SPDLOG_DEBUG("main: disk_monitor.stop_scan()");
    disk_monitor.stop_scan();

    stream_writer.deconfigure_scan();
    disk_monitor.deconfigure_scan();

    stream_writer.deconfigure_beam();
    disk_monitor.deconfigure_beam();

    dbw.unlock();
    dbw.disconnect();
    dbc.unlock_memory();
    dbc.destroy();
  }
  catch (std::exception& exc)
  {
    SPDLOG_ERROR("Exception caught: {}", exc.what());
    return_code = 1;
  }

  SPDLOG_DEBUG("return return_code={}", return_code);
  return return_code;
}

void usage()
{
  std::cout << "Usage: ska_pst_dsp_disk_perftest [options] config" << std::endl;
  std::cout << std::endl;
  std::cout << "  config      ascii file containing observation configuration" << std::endl;
  std::cout << "  -d path     write files to path [default /tmp]" << std::endl;
  std::cout << "  -h          print this help text" << std::endl;
  std::cout << "  -l nsecs    generate data stream for nsecs [default " << default_scan_len_max << "]" << std::endl;
  std::cout << "  -m          do not lock data block buffers into RAM [default false]" << std::endl;
  std::cout << "  -o          use O_DIRECT for file output" << std::endl;
  std::cout << "  -v          verbose output" << std::endl;
}

void signal_handler(int signalValue)
{
  SPDLOG_INFO("received signal {}", signalValue);
  if (quit_threads)
  {
    SPDLOG_WARN("received signal {} twice, exiting", signalValue);
    exit(EXIT_FAILURE);
  }
  quit_threads = 1;
}