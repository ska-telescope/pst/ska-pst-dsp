/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <unistd.h>
#include <ctime>
#include <iostream>
#include <stdexcept>
#include <ostream>
#include <spdlog/spdlog.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "ska/pst/dsp/disk/StreamWriter.h"

ska::pst::dsp::StreamWriter::StreamWriter(ska::pst::dsp::DiskMonitor& monitor) :
  ska::pst::common::ApplicationManager("StreamWriter"),
  disk_monitor(monitor),
  file_writer(false)
{
  SPDLOG_TRACE("ska::pst::dsp::StreamWriter::StreamWriter initialise");
  initialise();
}

ska::pst::dsp::StreamWriter::StreamWriter(ska::pst::dsp::DiskMonitor& monitor, bool use_o_direct) :
  ska::pst::common::ApplicationManager("StreamWriter"),
  disk_monitor(monitor),
  file_writer(use_o_direct)
{
  SPDLOG_TRACE("ska::pst::dsp::StreamWriter::StreamWriter initialise");
  initialise();
}

ska::pst::dsp::StreamWriter::~StreamWriter()
{
  SPDLOG_DEBUG("ska::pst::dsp::StreamWriter::~StreamWriter quit()");
  quit();
}

void ska::pst::dsp::StreamWriter::perform_initialise()
{
  SPDLOG_DEBUG("ska::pst::dsp::StreamWriter::perform_initialise");
}

void ska::pst::dsp::StreamWriter::validate_configure_beam(const ska::pst::common::AsciiHeader &config, ska::pst::common::ValidationContext *context)
{
  SPDLOG_TRACE("ska::pst::dsp::StreamWriter::validate_configure_beam() config={}", config.raw());
  // Always have these keys as this is called from DiskManager and the key is set
  std::string key = config.get_val("KEY");
  auto recording_base_path_str = config.get_val("RECORDING_BASE_PATH");
  auto stream_path_str = config.get_val("RECORDING_SUFFIX");

  SPDLOG_TRACE("ska::pst::dsp::StreamWriter::validate_configure_beam key={}", key);
  try {
    auto recording_base_path = std::filesystem::path(recording_base_path_str);
    SPDLOG_TRACE("ska::pst::dsp::StreamWriter::validate_configure_beam recording_base_path={}", recording_base_path.generic_string());
  } catch (std::exception& exc) {
    context->add_validation_error<std::string>("RECORDING_BASE_PATH", recording_base_path_str, std::string(exc.what()));
  }

  try {
    auto stream_path = std::filesystem::path(stream_path_str);
    SPDLOG_TRACE("ska::pst::dsp::StreamWriter::validate_configure_beam stream_path={}", stream_path.generic_string());
  } catch (std::exception& exc) {
    context->add_validation_error<std::string>("RECORDING_SUFFIX", stream_path_str, std::string(exc.what()));
  }
}

void ska::pst::dsp::StreamWriter::validate_configure_scan(const ska::pst::common::AsciiHeader &config, ska::pst::common::ValidationContext *context)
{
  SPDLOG_TRACE("ska::pst::dsp::StreamWriter::validation_configure_scan()");
  if (!config.has("BYTES_PER_SECOND")) {
    context->add_missing_field_error("BYTES_PER_SECOND");
  }

  if (!config.has("EB_ID")) {
    context->add_missing_field_error("EB_ID");
  }
}

void ska::pst::dsp::StreamWriter::validate_start_scan(const ska::pst::common::AsciiHeader& /*config*/)
{
  SPDLOG_TRACE("ska::pst::dsp::StreamWriter::validation_start_scan()");
}

void ska::pst::dsp::StreamWriter::perform_configure_beam()
{
  enforce_state(ska::pst::common::Idle, "cannot perform configure beam");

  // connect to the data block as a reader
  std::string key = beam_config.get_val("KEY");
  recording_base_path = std::filesystem::path(beam_config.get_val("RECORDING_BASE_PATH"));
  stream_path = std::filesystem::path(beam_config.get_val("RECORDING_SUFFIX"));
  timeout = 0;
  if (beam_config.has("TIMEOUT"))
  {
    timeout = beam_config.get_int32("TIMEOUT");
  }

  SPDLOG_DEBUG("ska::pst::dsp::StreamWriter::perform_configure_beam instantiating DataBlockRead({})", key);
  db = std::make_unique<ska::pst::smrb::DataBlockRead>(key);

  SPDLOG_DEBUG("ska::pst::dsp::StreamWriter::perform_configure_beam db->connect({})", timeout);
  db->connect(timeout);

  SPDLOG_DEBUG("ska::pst::dsp::StreamWriter::perform_configure_beam db->lock()");
  db->lock();

  header_bufsz = db->get_header_bufsz();
  data_bufsz = db->get_data_bufsz();

  // check that the header and data buffer sizes are compatible with o_direct flags
  file_writer.check_block_size(header_bufsz);
  file_writer.check_block_size(data_bufsz);
}

void ska::pst::dsp::StreamWriter::perform_configure_scan()
{
  enforce_state(ska::pst::common::BeamConfigured, "cannot perform configure scan");

  SPDLOG_DEBUG("ska::pst::dsp::StreamWriter::perform_configure_scan db->read_config()");
  db->read_config();

  // Configure the AsciiHeader from the data block's header
  SPDLOG_DEBUG("ska::pst::dsp::StreamWriter::perform_configure_scan scan_config.load_from_str(db->get_config())");
  scan_config.load_from_str(db->get_config());
  SPDLOG_DEBUG("ska::pst::dsp::StreamWriter::perform_configure_scan scan_config=\n{}", scan_config.raw());

  // ensure required header parameters are set
  scan_config.set("HDR_VERSION", "1.0");
  scan_config.set("HDR_SIZE", db->get_header_bufsz());

  bytes_per_second = scan_config.get_double("BYTES_PER_SECOND");
  resolution = std::max(scan_config.get_uint64("RESOLUTION"), db->get_data_bufsz());

  // initialise the first file_number in the output data stream
  file_number = 0;
  bufs_written_to_file = 0;

  // compute the bytes to be written to each file in the data stream
  bytes_written_per_file = uint64_t(floor(bytes_per_second * seconds_per_file));

  // ensure the bytes written per file is a multiple of the resolution
  uint64_t remainder = bytes_written_per_file % resolution;
  if (remainder > 0)
  {
    bytes_written_per_file += (resolution - remainder);
  }
  bufs_written_per_file = bytes_written_per_file / db->get_data_bufsz();
  SPDLOG_DEBUG("ska::pst::dsp::StreamWriter::perform_configure_scan bytes_written_per_file={} bufs_written_per_file", bytes_written_per_file, bufs_written_per_file);
  SPDLOG_DEBUG("ska::pst::dsp::StreamWriter::perform_configure_scan complete");
}

void ska::pst::dsp::StreamWriter::perform_start_scan()
{
  SPDLOG_TRACE("ska::pst::dsp::StreamWriter::perform_start_scan()");
  enforce_state(ska::pst::common::ScanConfigured, "cannot perform start scan");
}

void ska::pst::dsp::StreamWriter::perform_scan()
{
  enforce_state(ska::pst::common::Scanning, "StreamWriter cannot perform start scan");

  // wait for the header to be written to the db
  SPDLOG_DEBUG("ska::pst::dsp::StreamWriter::perform_scan db->read_header()");
  db->read_header();

  header.load_from_str(db->get_header());
  auto eb_id = std::filesystem::path(scan_config.get_val("EB_ID"));
  auto scan_id = std::filesystem::path(header.get_val("SCAN_ID"));

  // Load TELESCOPE from SMRB and convert to equivalent subsystem path
  auto telescope = scan_config.get_val("TELESCOPE");
  SPDLOG_DEBUG("ska::pst::dsp::StreamWriter::perform_configure_beam telescope={}", telescope);
  auto subsystem_id = std::filesystem::path(subsystem_path_map[telescope]);
  scan_path = recording_base_path / product_root_path / eb_id / subsystem_id / scan_id;
  utc_start = header.get_val("UTC_START");
  obs_offset = header.get_uint64("OBS_OFFSET");
  SPDLOG_DEBUG("ska::pst::dsp::StreamWriter::perform_scan SCAN_ID={} UTC_START={} OBS_OFFSET={}", header.get_val("SCAN_ID"), utc_start, obs_offset);

  SPDLOG_DEBUG("ska::pst::dsp::StreamWriter::perform_scan db->open()");
  db->open();

  output_path = scan_path / stream_path;
  SPDLOG_DEBUG("ska::pst::dsp::StreamWriter::perform_scan create_directories({})", output_path.generic_string());
  create_directories(output_path);

  bool eod = false;
  while (!eod)
  {
    SPDLOG_DEBUG("ska::pst::dsp::StreamWriter::perform_scan opening block");
    char * block_buffer = db->open_block();
    uint64_t bytes_read = db->get_buf_bytes();
    SPDLOG_DEBUG("ska::pst::dsp::StreamWriter::perform_scan opened block containing {} bytes", bytes_read);

    if (block_buffer == nullptr)
    {
      SPDLOG_DEBUG("ska::pst::dsp::StreamWriter::perform_scan encountered end of data");
      eod = true;
    }
    else
    {
      if (!file_writer.is_file_open())
      {
        std::filesystem::path file_name = file_writer.get_filename(utc_start, obs_offset, file_number);
        file_writer.open_file(output_path / file_name);
        header.set("OBS_OFFSET", obs_offset);
        header.set("FILE_NUMBER", file_number);
        ssize_t bytes_written = file_writer.write_header(header);
        disk_monitor.increment_bytes(bytes_written);
      }

      size_t bytes_written = file_writer.write_data(block_buffer, bytes_read);
      disk_monitor.increment_bytes(bytes_written);

      // increment the counters for bufs and bytes written to file
      bufs_written_to_file++;

      SPDLOG_DEBUG("ska::pst::dsp::StreamWriter::perform_scan write_data bufs_written={}/{} bytes_written={}", bufs_written_to_file, bufs_written_per_file, file_writer.get_data_bytes_written());

      if (bufs_written_to_file >= bufs_written_per_file)
      {
        file_writer.close_file();
        obs_offset += file_writer.get_data_bytes_written();
        bufs_written_to_file = 0;
        file_number++;
      }
      db->close_block(bytes_read);
    }
  }

  SPDLOG_DEBUG("ska::pst::dsp::StreamWriter::perform_scan EoD detected on input");
  if (file_writer.is_file_open())
  {
    file_writer.close_file();
  }

  SPDLOG_DEBUG("ska::pst::dsp::StreamWriter::perform_scan db->close()");
  db->close();
  mark_scan("scan_completed");
  header.reset();

  SPDLOG_DEBUG("ska::pst::dsp::StreamWriter::perform_scan returning");
}

void ska::pst::dsp::StreamWriter::mark_scan(const std::string& state)
{
  SPDLOG_DEBUG("ska::pst::dsp::StreamWriter::mark_scan(state={})", state);
  ska::pst::common::FileWriter fw(false);
  
  // create file
  std::filesystem::path file_name = std::filesystem::path(state);
  fw.open_file(scan_path / file_name);
  // close file
  fw.close_file();
}

void ska::pst::dsp::StreamWriter::perform_stop_scan()
{
  enforce_state(ska::pst::common::Scanning, "cannot perform stopscan");
  SPDLOG_DEBUG("ska::pst::dsp::StreamWriter::perform_stop_scan()");
  startscan_config.reset();
}

void ska::pst::dsp::StreamWriter::perform_deconfigure_scan()
{
  enforce_state(ska::pst::common::ScanConfigured, "cannot perform deconfigure scan");
  SPDLOG_DEBUG("ska::pst::dsp::StreamWriter::perform_deconfigure_scan");
  scan_config.reset();
}

void ska::pst::dsp::StreamWriter::perform_deconfigure_beam()
{
  enforce_state(ska::pst::common::BeamConfigured, "cannot perform deconfigure beam");
  SPDLOG_DEBUG("ska::pst::dsp::StreamWriter::perform_deconfigure_beam resetting bufsz");
  header_bufsz = 0;
  data_bufsz = 0;

  SPDLOG_DEBUG("ska::pst::dsp::StreamWriter::perform_deconfigure_beam db->unlock()");
  db->unlock();

  SPDLOG_DEBUG("ska::pst::dsp::StreamWriter::perform_deconfigure_beam db->disconnect()");
  db->disconnect();

  SPDLOG_DEBUG("ska::pst::dsp::StreamWriter::perform_deconfigure_beam file_writer.deconfigure()");
  file_writer.deconfigure();

  SPDLOG_DEBUG("ska::pst::dsp::StreamWriter::perform_deconfigure_beam db.reset()");
  db.reset();
  beam_config.reset();
}
