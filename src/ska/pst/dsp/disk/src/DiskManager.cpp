/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <spdlog/spdlog.h>

#include "ska/pst/dsp/disk/DiskManager.h"
#include "ska/pst/common/utils/AsciiHeader.h"

ska::pst::dsp::DiskManager::DiskManager(const std::string& base_path, bool use_o_direct) :
  ska::pst::common::ApplicationManager(""),
  disk_monitor(base_path),
  recording_base_path(base_path),
  o_direct(use_o_direct)
{
  SPDLOG_DEBUG("ska::pst::dsp::DiskManager::DiskManager({})", base_path);
  initialise();
}

ska::pst::dsp::DiskManager::~DiskManager()
{
  SPDLOG_DEBUG("ska::pst::dsp::DiskManager::~DiskManager quit()");
  quit();
}

void ska::pst::dsp::DiskManager::perform_initialise()
{
  SPDLOG_DEBUG("ska::pst::dsp::DiskManager::perform_initialise()");
  data_stream = std::make_unique<ska::pst::dsp::StreamWriter>(disk_monitor, o_direct);
  weights_stream = std::make_unique<ska::pst::dsp::StreamWriter>(disk_monitor, o_direct);
}

void ska::pst::dsp::DiskManager::configure_from_file(const std::string &config_file)
{
  SPDLOG_DEBUG("ska::pst::dsp::DiskManager::configure_from_file config_file={}",config_file);
  ska::pst::common::AsciiHeader config;
  config.load_from_file(config_file);
  SPDLOG_DEBUG("ska::pst::dsp::DiskManager::configure_from_file config={}",config.raw());

  // configure beam
  ska::pst::common::AsciiHeader beam_config;
  // recording path
  beam_config.set_val("DATA_KEY", config.get_val("DATA_KEY"));
  beam_config.set_val("WEIGHTS_KEY", config.get_val("WEIGHTS_KEY"));
  configure_beam(beam_config);

  // configure scan
  ska::pst::common::AsciiHeader scan_config;
  scan_config.set_val("BYTES_PER_SECOND", config.get_val("BYTES_PER_SECOND"));
  scan_config.set_val("SCANLEN_MAX", config.get_val("SCANLEN_MAX"));
  scan_config.set_val("EB_ID", config.get_val("EB_ID"));
  configure_scan(scan_config);

  // configure from file
  ska::pst::common::AsciiHeader start_scan_config;
  start_scan_config.set_val("SCAN_ID", config.get_val("SCAN_ID"));
  start_scan(start_scan_config);
}

void ska::pst::dsp::DiskManager::validate_configure_beam(const ska::pst::common::AsciiHeader& config, ska::pst::common::ValidationContext *context)
{
  SPDLOG_DEBUG("ska::pst::dsp::DiskManager::validate_configure_beam config={}", config.raw());  
  disk_monitor.validate_configure_beam(config, context);

  if (config.has("DATA_KEY"))
  {
    SPDLOG_DEBUG("ska::pst::dsp::DiskManager::validate_configure_beam data_stream->validate_configure_beam(data_beam_config)");
    // don't assign to the instance variable - this helps having to avoid rolling back when validation errors occur
    ska::pst::common::AsciiHeader data_beam_config;
    data_beam_config.set_val("KEY", config.get_val("DATA_KEY"));
    data_beam_config.set_val("RECORDING_BASE_PATH", recording_base_path);
    data_beam_config.set_val("RECORDING_SUFFIX", "data");
    data_stream->validate_configure_beam(data_beam_config, context);
  } else {
    context->add_missing_field_error("DATA_KEY");
  }

  if (config.has("WEIGHTS_KEY"))
  {
    SPDLOG_DEBUG("ska::pst::dsp::DiskManager::validate_configure_beam weights_stream->validate_configure_beam(weights_beam_config)");
    // don't assign to the instance variable - this helps having to avoid rolling back when validation errors occur
    ska::pst::common::AsciiHeader data_beam_config;
    weights_beam_config.set_val("KEY", config.get_val("WEIGHTS_KEY"));
    weights_beam_config.set_val("RECORDING_BASE_PATH", recording_base_path);
    weights_beam_config.set_val("RECORDING_SUFFIX", "weights");
    weights_stream->validate_configure_beam(weights_beam_config, context);
  } else {
    context->add_missing_field_error("WEIGHTS_KEY");
  }

  SPDLOG_DEBUG("ska::pst::dsp::DiskManager::validate_configure_beam complete");
}

void ska::pst::dsp::DiskManager::validate_configure_scan(const ska::pst::common::AsciiHeader& config, ska::pst::common::ValidationContext *context)
{
  SPDLOG_DEBUG("ska::pst::dsp::DiskManager::validate_configure_scan");
  disk_monitor.validate_configure_scan(config, context);
  data_stream->validate_configure_scan(config, context);
  weights_stream->validate_configure_scan(config, context);
  SPDLOG_DEBUG("ska::pst::dsp::DiskManager::validate_configure_scan done");
}

void ska::pst::dsp::DiskManager::validate_start_scan(const ska::pst::common::AsciiHeader& /*config*/)
{
  SPDLOG_DEBUG("ska::pst::dsp::DiskManager::validate_start_scan");
}

void ska::pst::dsp::DiskManager::perform_configure_beam()
{
  SPDLOG_DEBUG("ska::pst::dsp::DiskManager::perform_configure_beam beam_config={}", beam_config.raw());
  enforce(!is_beam_configured(), "DiskManager perform_configure_beam cannot be called when beam already configured");
  disk_monitor.configure_beam(beam_config);

  try
  {
    data_beam_config.set_val("KEY", beam_config.get_val("DATA_KEY"));
    data_beam_config.set_val("RECORDING_BASE_PATH", recording_base_path);
    data_beam_config.set_val("RECORDING_SUFFIX", "data");
    SPDLOG_TRACE("ska::pst::dsp::DiskManager::perform_configure_beam data_beam_config={}", data_beam_config.raw());
    data_stream->configure_beam(data_beam_config);
  }
  catch(const std::exception& e)
  {
    SPDLOG_WARN("ska::pst::dsp::DiskManager::perform_configure_beam exception during data_stream->configure_beam(data_beam_config) {}", e.what());
    disk_monitor.deconfigure_beam();
    throw(e);
  }

  try
  {
    weights_beam_config.set_val("KEY", beam_config.get_val("WEIGHTS_KEY"));
    weights_beam_config.set_val("RECORDING_BASE_PATH", recording_base_path);
    weights_beam_config.set_val("RECORDING_SUFFIX", "weights");
    SPDLOG_TRACE("ska::pst::dsp::DiskManager::perform_configure_beam weights_beam_config={}", weights_beam_config.raw());
    weights_stream->configure_beam(weights_beam_config);
  }
  catch(const std::exception& e)
  {
    SPDLOG_WARN("ska::pst::dsp::DiskManager::perform_configure_beam exception duringweights_stream->configure_beam(weights_beam_config) {}", e.what());
    disk_monitor.deconfigure_beam();
    data_stream->deconfigure_beam();
    throw(e);
  }
}

void ska::pst::dsp::DiskManager::perform_configure_scan()
{
  SPDLOG_DEBUG("ska::pst::dsp::DiskManager::perform_configure_scan disk_monitor.configure_scan()");
  disk_monitor.configure_scan(scan_config);

  try
  {
    data_stream->configure_scan(scan_config);
  }
  catch(const std::exception& e)
  {
    SPDLOG_WARN("ska::pst::dsp::DiskManager::perform_configure_scan exception during data_stream->configure_scan(scan_config) {}", e.what());
    disk_monitor.deconfigure_scan();
    throw(e);
  }

  try
  {
    weights_stream->configure_scan(scan_config);
  }
  catch(const std::exception& e)
  {
    SPDLOG_WARN("ska::pst::dsp::DiskManager::perform_configure_scan exception during weights_stream->configure_scan(scan_config) {}", e.what());
    disk_monitor.deconfigure_scan();
    data_stream->deconfigure_scan();
    throw(e);
  }

  uint64_t bufs_per_file = data_stream->get_bufs_written_per_file();
  weights_stream->set_bufs_written_per_file(bufs_per_file);

  SPDLOG_DEBUG("ska::pst::dsp::DiskManager::perform_configure_scan done");
}

void ska::pst::dsp::DiskManager::perform_start_scan()
{
  SPDLOG_DEBUG("ska::pst::dsp::DiskManager::perform_start_scan");
  disk_monitor.start_scan(startscan_config);
  data_stream->start_scan(startscan_config);
  weights_stream->start_scan(startscan_config);
}

void ska::pst::dsp::DiskManager::perform_scan()
{
  SPDLOG_DEBUG("ska::pst::dsp::DiskManager::perform_scan");
}

void ska::pst::dsp::DiskManager::perform_stop_scan()
{
  SPDLOG_DEBUG("ska::pst::dsp::DiskManager::perform_stop_scan weights_stream->stop_scan()");
  weights_stream->stop_scan();
  SPDLOG_DEBUG("ska::pst::dsp::DiskManager::perform_stop_scan data_stream->stop_scan()");
  data_stream->stop_scan();
  SPDLOG_DEBUG("ska::pst::dsp::DiskManager::perform_stop_scan disk_monitor.stop_scan()");
  disk_monitor.stop_scan();
  SPDLOG_DEBUG("ska::pst::dsp::DiskManager::perform_stop_scan done");
}

void ska::pst::dsp::DiskManager::perform_deconfigure_scan()
{
  SPDLOG_DEBUG("ska::pst::dsp::DiskManager::perform_deconfigure_scan");
  weights_stream->deconfigure_scan();
  data_stream->deconfigure_scan();
  disk_monitor.deconfigure_scan();
  SPDLOG_DEBUG("ska::pst::dsp::DiskManager::perform_deconfigure_scan done");
}

void ska::pst::dsp::DiskManager::perform_deconfigure_beam()
{
  weights_stream->deconfigure_beam();
  data_stream->deconfigure_beam();
  disk_monitor.deconfigure_beam();
}

auto ska::pst::dsp::DiskManager::get_beam_configuration() -> ska::pst::common::AsciiHeader &
{
  SPDLOG_DEBUG("ska::pst::dsp::DiskManager::get_beam_configuration()");
  enforce(is_beam_configured(), "cannot get beam configuration as the beam has not been configured");
  return beam_config;
}

auto ska::pst::dsp::DiskManager::get_scan_configuration() -> ska::pst::common::AsciiHeader &
{
  SPDLOG_DEBUG("ska::pst::dsp::DiskManager::get_scan_configuration()");
  enforce(is_scan_configured(), "cannot get scan configuration when scan has not be configured");
  return scan_config;
}

auto ska::pst::dsp::DiskManager::get_disk_stats() -> ska::pst::dsp::DiskManager::stats_t
{
  ska::pst::dsp::DiskManager::stats_t stats;
  stats.capacity = disk_monitor.get_disk_capacity();
  stats.available = disk_monitor.get_disk_available();
  // following values are only valid if a scan is configured
  if (is_scan_configured())
  {
    stats.bytes_written = disk_monitor.get_bytes_written();
    stats.expected_data_write_rate = disk_monitor.get_expected_data_write_rate();
  }
  if (is_scanning())
  {
    stats.data_write_rate = disk_monitor.get_data_write_rate();
  }
  return stats;
}
