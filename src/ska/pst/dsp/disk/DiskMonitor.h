/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <cstddef>
#include <mutex>
#include <inttypes.h>

#include "ska/pst/common/statemodel/ApplicationManager.h"
#include "ska/pst/common/utils/AsciiHeader.h"
#include "ska/pst/common/utils/Timer.h"
#include "ska/pst/common/utils/ValidationContext.h"

#ifndef SKA_PST_DSP_DISK_DiskMonitor_h
#define SKA_PST_DSP_DISK_DiskMonitor_h

namespace ska::pst::dsp {

  /**
   * @brief Monitors the disk system used for file writing
   *
   */
  class DiskMonitor : public ska::pst::common::ApplicationManager {

    public:

      /**
       * @brief Construct a new Disk Monitor object.
       *
       * @param recording_path file system to be monitored
       */
      DiskMonitor(std::string recording_path);

      /**
       * @brief Destroy the DiskMonitor object
       *
       */
      ~DiskMonitor();

      /**
       * @brief Configure beam as described by the configuration parameters
       *
       * @param config beam configuration containing the recording path, data and weights key
       * @param context A validation context where errors should be added.
       */
      void validate_configure_beam(const ska::pst::common::AsciiHeader& config, ska::pst::common::ValidationContext *context);

      /**
       * @brief Configure scan as described by the configuration parameters
       *
       * @param config scan configuration containing the bytes_per_second and scan_len_max
       * @param context A validation context where errors should be added.
       */
      void validate_configure_scan(const ska::pst::common::AsciiHeader& config, ska::pst::common::ValidationContext *context);

      /**
       * @brief Start the scan using the provided configuration parameters
       *
       * @param config Scan runtime configuration parameters
       */
      void validate_start_scan(const ska::pst::common::AsciiHeader& config);

      /**
       * @brief Increment bytes_written by the specified number of bytes.
       *
       * @param nbytes Number of bytes to increment
       * @throw std::runtime_error if scanning is false
       */
      void increment_bytes(uint64_t nbytes);

      /**
       * @brief Return the amount of data of data written in the current scan.
       *
       * @return uint64_t Number of bytes written.
       * @throw std::runtime_error if configured is false
       */
      uint64_t get_bytes_written() const;

      /**
       * @brief Get the current data write rate.
       *
       * @return double data write rate in bytes per second
       * @throw std::runtime_error if scanning is false
       */
      double get_data_write_rate() const;

      /**
       * @brief Get the expected data rate based on the configuration
       *
       * @return double expected data rate in bytes per second
       * @throw std::runtime_error if configured is false
       */
      double get_expected_data_write_rate() const;

      /**
       * @brief Return the capacity of the file system.
       *
       * @return uint64_t total capacity of the file system in bytes.
       * @throw std::runtime_error if resources_assigned is false
       */
      size_t get_disk_capacity() const;

      /**
       * @brief Get the available space of the file system
       *
       * @return float data write rate in bytes per second
       * @throw std::runtime_error if resources_assigned is false
       */
      size_t get_disk_available() const;

      /**
       * @brief Get the available recording time base on the data rate and available disk space.
       *
       * @return double available recording time in seconds
       * @throw std::runtime_error if configured is false
       */
      double get_recording_time_available() const;

    private:

      void perform_initialise();
      void perform_configure_beam();
      void perform_configure_scan();
      void perform_start_scan();
      void perform_scan();
      void perform_stop_scan();
      void perform_deconfigure_scan();
      void perform_deconfigure_beam();
      void perform_reset();
      void perform_terminate();

      /**
       * @brief Monitor the recording filesystem path to determine the file system capacity and available space
       *
       * @throw std::filesystem::filesystem_error if the file_system attribute is an invalid path.
       */
      void monitor_filesystem();

      /**
       * @brief Query the file system at the file_system path and retrieve the capacity and available disk space
       *
       * @throw std::filesystem::filesystem_error if the file_system attribute is an invalid path.
       */
      void query_filesystem();

      //! monitoring thread that commences backing file system monitoring upon construction
      std::unique_ptr<std::thread> monitor_thread{nullptr};

      //! the configuration parameters provided in the configure_scan command
      ska::pst::common::AsciiHeader header;

      //! Timer that records the elapsed time between calculates of the data rate
      ska::pst::common::Timer file_writer_timer{};

      //! File system to be monitored for available space
      std::string file_system;

      //! Total size of the file system being monitored
      std::intmax_t file_system_capacity{0};

      //! Available space on the file system being monitored
      std::intmax_t file_system_available{0};

      //! Available space on the file system at the previous monitoring point
      std::intmax_t prev_file_system_available{0};

      //! Change in the available space on the file system in bytes per second
      double file_system_available_rate{0};

      //! Recording time available given the expected data rate in bytes per second
      double recording_time_available{0};

      //! Total number of bytes written
      uint64_t bytes_written{0};

      //! Number of bytes written when the timer was reset
      uint64_t prev_bytes_written{0};

      //! Instantaneous data rate for bytes written per second
      double bytes_written_rate{0};

      //! Minimum elapsed time between sampling in seconds
      double minimum_elapsed{0.001};

      //! The exepected data rate in bytes per second
      double expected_bytes_per_second{0};

      //! Length of the configured scan in seconds
      double scan_len{0};

      //! flag to control the file system monitoring thread
      volatile bool monitoring{false};

      //! flag to control the file perform_scan thread
      volatile bool performing_scan{false};

      //! Timer for recording elapsed time between file system monitoring checks
      ska::pst::common::Timer monitor_filesystem_timer{};

      //! Condition variable to coordinate access to the control attributes
      std::condition_variable cond;

      //! Mutex to prevent multiple threads from updating the timer
      std::mutex mutex;

  };

} // namespace ska::pst::dsp

#endif // SKA_PST_DSP_DISK_DiskMonitor_h
