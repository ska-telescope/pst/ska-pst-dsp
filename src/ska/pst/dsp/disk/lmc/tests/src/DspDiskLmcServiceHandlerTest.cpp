/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <iostream>
#include <spdlog/spdlog.h>
#include <grpc/grpc.h>
#include <grpc++/grpc++.h>

#include "ska/pst/common/lmc/LmcServiceException.h"
#include "ska/pst/dsp/disk/lmc/tests/DspDiskLmcServiceHandlerTest.h"
#include "ska/pst/dsp/testutils/GtestMain.h"

auto main(int argc, char* argv[]) -> int
{
  return ska::pst::dsp::test::gtest_main(argc, argv);
}

namespace ska::pst::dsp::test {

DspDiskLmcServiceHandlerTest::DspDiskLmcServiceHandlerTest()
    : ::testing::Test()
{
}

void DspDiskLmcServiceHandlerTest::SetUp()
{
    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::SetUp()");
    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::SetUp creating shared data block manager");
    _dsp = std::make_shared<ska::pst::dsp::DiskManager>("/tmp", false);

    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::SetUp creating handler");
    handler = std::make_shared<ska::pst::dsp::DspDiskLmcServiceHandler>(_dsp);

    helper.setup();
}

void DspDiskLmcServiceHandlerTest::TearDown()
{
    if (_dsp->is_scanning())
    {
        SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::TearDown _dsp->stop_scan()");
        _dsp->stop_scan();
    }

    if (_dsp->is_scan_configured())
    {
        SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::TearDown _dsp->deconfigure_scan()");
        _dsp->deconfigure_scan();
    }

    if (_dsp->is_beam_configured())
    {
        SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::TearDown _dsp->deconfigure_beam()");
        _dsp->deconfigure_beam();
    }

    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::TearDown complete");
}

void DspDiskLmcServiceHandlerTest::configure_beam()
{
    ska::pst::lmc::BeamConfiguration request;
    auto dsp_resources_request = request.mutable_dsp_disk();
    dsp_resources_request->set_data_key(helper.data_key);
    dsp_resources_request->set_weights_key(helper.weights_key);

    handler->configure_beam(request);
}

void DspDiskLmcServiceHandlerTest::configure_scan()
{
    ska::pst::lmc::ScanConfiguration request;

    auto *dsp_scan_configuration_request = request.mutable_dsp_disk();
    dsp_scan_configuration_request->set_bytes_per_second(helper.data_scan_config.get_double("BYTES_PER_SECOND"));
    dsp_scan_configuration_request->set_scanlen_max(helper.data_scan_config.get_double("SCANLEN_MAX"));
    dsp_scan_configuration_request->set_execution_block_id(helper.data_scan_config.get_val("EB_ID"));

    handler->configure_scan(request);
}

void DspDiskLmcServiceHandlerTest::start_scan()
{
    handler->start_scan(ska::pst::lmc::StartScanRequest());
}

TEST_F(DspDiskLmcServiceHandlerTest, configure_deconfigure_beam) // NOLINT
{
    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::configure_deconfigure_beam - configuring beam");
    EXPECT_FALSE(handler->is_beam_configured()); // NOLINT
    EXPECT_FALSE(_dsp->is_beam_configured());

    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::configure_deconfigure_beam - configure_beam");
    configure_beam();

    EXPECT_TRUE(handler->is_beam_configured()); // NOLINT
    EXPECT_TRUE(_dsp->is_beam_configured());
    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::configure_deconfigure_beam - beam configured");

    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::configure_deconfigure_beam - getting beam configuration");
    ska::pst::lmc::BeamConfiguration beam_configuration;
    handler->get_beam_configuration(&beam_configuration);

    EXPECT_TRUE(beam_configuration.has_dsp_disk()); // NOLINT
    auto dsp_beam_configuration = beam_configuration.dsp_disk();

    EXPECT_EQ(helper.data_key, dsp_beam_configuration.data_key()); // NOLINT
    EXPECT_EQ(helper.weights_key, dsp_beam_configuration.weights_key()); // NOLINT

    ska::pst::common::AsciiHeader &dsp_resources = _dsp->get_beam_configuration();
    EXPECT_EQ(helper.data_key, dsp_resources.get_val("DATA_KEY")); // NOLINT
    EXPECT_EQ(helper.weights_key, dsp_resources.get_val("WEIGHTS_KEY")); // NOLINT
    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::configure_deconfigure_beam - checked beam configuration");

    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::configure_deconfigure_beam - deconfiguring beam");
    handler->deconfigure_beam();
    EXPECT_FALSE(handler->is_beam_configured()); // NOLINT
    EXPECT_FALSE(_dsp->is_beam_configured()); // NOLINT
    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::configure_deconfigure_beam - beam deconfigured");
}

TEST_F(DspDiskLmcServiceHandlerTest, configure_beam_again_should_throw_exception) // NOLINT
{
    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::configure_beam_again_should_throw_exception - configuring beam");

    EXPECT_FALSE(handler->is_beam_configured()); // NOLINT

    configure_beam();

    EXPECT_TRUE(handler->is_beam_configured()); // NOLINT
    EXPECT_TRUE(_dsp->is_beam_configured()); // NOLINT
    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::configure_beam_again_should_throw_exception - beam configured");

    try {
        configure_beam();
        FAIL() << " expected configure_beam to throw exception due to beam configured already.\n";
    } catch (ska::pst::common::LmcServiceException& ex) {
        EXPECT_EQ(std::string(ex.what()), "Beam already configured for DSP.DISK.");
        EXPECT_EQ(ex.error_code(), ska::pst::lmc::ErrorCode::CONFIGURED_FOR_BEAM_ALREADY);
        EXPECT_EQ(ex.status_code(), grpc::StatusCode::FAILED_PRECONDITION);
    }
}

TEST_F(DspDiskLmcServiceHandlerTest, configure_beam_with_invalid_configuration) // NOLINT
{
    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::configure_beam_with_invalid_configuration - configuring beam");
    helper.data_key = "abcd";

    EXPECT_FALSE(handler->is_beam_configured());
    try {
        configure_beam();
        FAIL() << " expected configure_beam to throw exception due to invalid configuration.\n";
    } catch (std::exception& ex) {
        SPDLOG_DEBUG("DspDiskLmcServiceHandlerTest::configure_beam_with_invalid_configuration exception thrown as expected: {}", ex.what());
        EXPECT_EQ(_dsp->get_state(), ska::pst::common::State::RuntimeError);
        EXPECT_FALSE(handler->is_beam_configured());
    }
    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::configure_beam_with_invalid_configuration test done");
}


TEST_F(DspDiskLmcServiceHandlerTest, configure_beam_should_have_dsp_object) // NOLINT
{
    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::configure_beam_should_have_dsp_object - configuring beam");
    EXPECT_FALSE(handler->is_beam_configured()); // NOLINT

    ska::pst::lmc::BeamConfiguration beam_configuration;
    beam_configuration.mutable_test();

    try {
        handler->configure_beam(beam_configuration);
        FAIL() << " expected configure_beam to throw exception due not having DSP.DISK field.\n";
    } catch (ska::pst::common::LmcServiceException& ex) {
        EXPECT_EQ(std::string(ex.what()), "Expected a DSP.DISK beam configuration object, but none were provided.");
        EXPECT_EQ(ex.error_code(), ska::pst::lmc::ErrorCode::INVALID_REQUEST);
        EXPECT_EQ(ex.status_code(), grpc::StatusCode::INVALID_ARGUMENT);
    }
}

TEST_F(DspDiskLmcServiceHandlerTest, get_beam_configuration_when_not_beam_configured) // NOLINT
{
    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::get_beam_configuration_when_not_beam_configured - getting beam configuration");
    EXPECT_FALSE(handler->is_beam_configured());

    ska::pst::lmc::BeamConfiguration response;
    try {
        handler->get_beam_configuration(&response);
    } catch (ska::pst::common::LmcServiceException& ex) {
        EXPECT_EQ(std::string(ex.what()), "DSP.DISK not configured for beam.");
        EXPECT_EQ(ex.error_code(), ska::pst::lmc::ErrorCode::NOT_CONFIGURED_FOR_BEAM);
        EXPECT_EQ(ex.status_code(), grpc::StatusCode::FAILED_PRECONDITION);
    }
    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::get_beam_configuration_when_not_beam_configured - beam not configured");
}

TEST_F(DspDiskLmcServiceHandlerTest, deconfigure_beam_when_not_beam_configured) // NOLINT
{
    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::deconfigure_beam_when_not_beam_configured - deconfiguring beam");

    EXPECT_FALSE(handler->is_beam_configured()); // NOLINT

    try {
        handler->deconfigure_beam();
        FAIL() << " expected deconfigure_beam to throw exception due to beam not configured.\n";
    } catch (ska::pst::common::LmcServiceException& ex) {
        EXPECT_EQ(std::string(ex.what()), "DSP.DISK not configured for beam.");
        EXPECT_EQ(ex.error_code(), ska::pst::lmc::ErrorCode::NOT_CONFIGURED_FOR_BEAM);
        EXPECT_EQ(ex.status_code(), grpc::StatusCode::FAILED_PRECONDITION);
    }
}

TEST_F(DspDiskLmcServiceHandlerTest, deconfigure_beam_when_scan_configured) // NOLINT
{
    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::deconfigure_beam_when_scan_configured - configuring beam");
    configure_beam();
    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::deconfigure_beam_when_scan_configured - beam configured");

    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::deconfigure_beam_when_scan_configured - configuring scan");
    EXPECT_FALSE(handler->is_scan_configured());
    configure_scan();
    EXPECT_TRUE(handler->is_scan_configured());
    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::deconfigure_beam_when_scan_configured - scan configured");

    try {
        handler->deconfigure_beam();
        FAIL() << " expected deconfigure_beam to throw exception due having scan configuration.\n";
    } catch (ska::pst::common::LmcServiceException& ex) {
        SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::deconfigure_beam_when_scan_configured exception occured as expected");
        EXPECT_EQ(std::string(ex.what()), "DSP.DISK is configured for scan but trying to deconfigure beam.");
        EXPECT_EQ(ex.error_code(), ska::pst::lmc::ErrorCode::INVALID_REQUEST);
        EXPECT_EQ(ex.status_code(), grpc::StatusCode::FAILED_PRECONDITION);
    }
    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::deconfigure_beam_when_scan_configured done");
}

TEST_F(DspDiskLmcServiceHandlerTest, configure_deconfigure_scan) // NOLINT
{

    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::configure_deconfigure_scan - configuring beam");
    configure_beam();
    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::configure_deconfigure_scan - beam configured");

    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::configure_deconfigure_scan - configuring scan");
    EXPECT_FALSE(handler->is_scan_configured());
    configure_scan();
    EXPECT_TRUE(handler->is_scan_configured());
    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::configure_deconfigure_scan - scan configured");

    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::configure_deconfigure_scan - getting configuration");
    ska::pst::lmc::ScanConfiguration get_response;
    handler->get_scan_configuration(&get_response);
    EXPECT_TRUE(get_response.has_dsp_disk()); // NOLINT

    const auto &dsp_scan_configuration = get_response.dsp_disk();
    EXPECT_EQ(helper.data_scan_config.get_double("BYTES_PER_SECOND"), dsp_scan_configuration.bytes_per_second());
    EXPECT_EQ(helper.data_scan_config.get_double("SCANLEN_MAX"), dsp_scan_configuration.scanlen_max());
    EXPECT_EQ(helper.data_scan_config.get_val("EB_ID"), dsp_scan_configuration.execution_block_id());

    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::configure_deconfigure_scan - checked configuration");

    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::configure_deconfigure_scan - deconfiguring scan");
    handler->deconfigure_scan();
    EXPECT_FALSE(handler->is_scan_configured());
    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::configure_deconfigure_scan - scan deconfigured");

    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::configure_deconfigure_scan - deconfiguring beam");
    handler->deconfigure_beam();
    EXPECT_FALSE(handler->is_beam_configured()); // NOLINT
    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::configure_deconfigure_scan - beam deconfigured");
}

TEST_F(DspDiskLmcServiceHandlerTest, configure_scan_with_invalid_configuration) // NOLINT
{
    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::configure_scan_with_invalid_configuration - configuring beam");
    configure_beam();
    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::configure_scan_with_invalid_configuration - beam configured");

    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::configure_scan_with_invalid_configuration - configuring scan");
    EXPECT_FALSE(handler->is_scan_configured());
    static constexpr double bad_double = 1e24;
    helper.data_scan_config.set("BYTES_PER_SECOND", bad_double);
    helper.data_scan_config.set("SCANLEN_MAX", bad_double);
    try {
        configure_scan();
        FAIL() << " expected configure_scan to throw exception due to not having beam configured.\n";
    } catch (std::exception& ex) {
        EXPECT_EQ(_dsp->get_state(), ska::pst::common::State::BeamConfigured);
        EXPECT_FALSE(handler->is_scan_configured());
        EXPECT_TRUE(handler->is_beam_configured());

        SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::configure_scan_with_invalid_configuration deconfiguring beam");
        handler->deconfigure_beam();
        EXPECT_EQ(_dsp->get_state(), ska::pst::common::State::Idle);
        SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::configure_scan_with_invalid_configuration deconfiguring beam");
    }
}

TEST_F(DspDiskLmcServiceHandlerTest, configure_scan_when_not_beam_configured) // NOLINT
{
    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::configure_scan_when_not_beam_configured - configuring scan");

    try {
        configure_scan();
        FAIL() << " expected configure_scan to throw exception due to not having beam configured.\n";
    } catch (ska::pst::common::LmcServiceException& ex) {
        EXPECT_EQ(std::string(ex.what()), "DSP.DISK not configured for beam.");
        EXPECT_EQ(ex.error_code(), ska::pst::lmc::ErrorCode::NOT_CONFIGURED_FOR_BEAM);
        EXPECT_EQ(ex.status_code(), grpc::StatusCode::FAILED_PRECONDITION);
    }
}

TEST_F(DspDiskLmcServiceHandlerTest, configure_scan_again_should_throw_exception) // NOLINT
{
    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::configure_scan_again_should_throw_exception - configuring beam");
    EXPECT_FALSE(handler->is_beam_configured()); // NOLINT
    configure_beam();
    EXPECT_TRUE(handler->is_beam_configured()); // NOLINT
    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::configure_scan_again_should_throw_exception - beam configured");

    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::configure_scan_again_should_throw_exception - configuring scan");
    EXPECT_FALSE(handler->is_scan_configured());
    configure_scan();
    EXPECT_TRUE(handler->is_scan_configured());
    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::configure_scan_again_should_throw_exception - scan configured");

    try {
        configure_scan();
        FAIL() << " expected configure_scan to throw exception due to scan already configured.\n";
    } catch (ska::pst::common::LmcServiceException& ex) {
        EXPECT_EQ(std::string(ex.what()), "Scan already configured for DSP.DISK.");
        EXPECT_EQ(ex.error_code(), ska::pst::lmc::ErrorCode::CONFIGURED_FOR_SCAN_ALREADY);
        EXPECT_EQ(ex.status_code(), grpc::StatusCode::FAILED_PRECONDITION);
    }
}

TEST_F(DspDiskLmcServiceHandlerTest, configure_scan_should_have_dsp_object) // NOLINT
{
    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::configure_scan_should_have_dsp_object - configuring beam");
    configure_beam();
    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::configure_scan_should_have_dsp_object - beam configured");

    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::configure_scan_should_have_dsp_object - configuring scan");
    try {
        ska::pst::lmc::ScanConfiguration configuration;
        configuration.mutable_test();
        handler->configure_scan(configuration);
        FAIL() << " expected configure_scan to throw exception due not having DSP.DISK field.\n";
    } catch (ska::pst::common::LmcServiceException& ex) {
        EXPECT_EQ(std::string(ex.what()), "Expected a DSP.DISK scan configuration object, but none were provided.");
        EXPECT_EQ(ex.error_code(), ska::pst::lmc::ErrorCode::INVALID_REQUEST);
        EXPECT_EQ(ex.status_code(), grpc::StatusCode::INVALID_ARGUMENT);
    }
}

TEST_F(DspDiskLmcServiceHandlerTest, deconfigure_scan_when_not_configured) // NOLINT
{
    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::deconfigure_scan_when_not_configured - deconfiguring scan");

    try {
        handler->deconfigure_scan();
        FAIL() << " expected deconfigure_scan to throw exception due to beam not configured.\n";
    } catch (ska::pst::common::LmcServiceException& ex) {
        EXPECT_EQ(std::string(ex.what()), "Scan not currently configured for DSP.DISK.");
        EXPECT_EQ(ex.error_code(), ska::pst::lmc::ErrorCode::NOT_CONFIGURED_FOR_SCAN);
        EXPECT_EQ(ex.status_code(), grpc::StatusCode::FAILED_PRECONDITION);
    }
}

TEST_F(DspDiskLmcServiceHandlerTest, get_scan_configuration_when_not_configured) // NOLINT
{
    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::get_scan_configuration_when_not_configured - getting scan configuration"); // NOLINT

    try {
        ska::pst::lmc::ScanConfiguration scan_configuration;
        handler->get_scan_configuration(&scan_configuration);
        FAIL() << " expected deconfigure_beam to throw exception due to beam not configured.\n";
    } catch (ska::pst::common::LmcServiceException& ex) {
        EXPECT_EQ(std::string(ex.what()), "Not currently configured for DSP.DISK.");
        EXPECT_EQ(ex.error_code(), ska::pst::lmc::ErrorCode::NOT_CONFIGURED_FOR_SCAN);
        EXPECT_EQ(ex.status_code(), grpc::StatusCode::FAILED_PRECONDITION);
    }
}

TEST_F(DspDiskLmcServiceHandlerTest, start_scan_stop_scan) // NOLINT
{
    helper.write_data();

    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::start_scan_stop_scan - configuring beam");
    configure_beam();
    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::start_scan_stop_scan - beam configured");

    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::start_scan_stop_scan - configuring scan");
    configure_scan();
    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::start_scan_stop_scan - scan configured");

    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::start_scan_stop_scan - starting scan");
    EXPECT_FALSE(handler->is_scanning());
    start_scan();
    EXPECT_TRUE(handler->is_scanning());

    // sleep for bit, want to wait so we can stop.
    usleep(microseconds_per_decisecond);
    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::start_scan_stop_scan - scanning");

    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::start_scan_stop_scan - ending scan");
    handler->stop_scan();
    EXPECT_FALSE(handler->is_scanning());
    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::start_scan_stop_scan - scan ended");

    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::start_scan_stop_scan - deconfiguring scan");
    handler->deconfigure_scan();
    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::start_scan_stop_scan - scan deconfigured");

    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::start_scan_stop_scan - deconfiguring beam");
    handler->deconfigure_beam();
    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::start_scan_stop_scan - beam deconfigured");
}

TEST_F(DspDiskLmcServiceHandlerTest, start_scan_when_scanning) // NOLINT
{
    helper.write_data();

    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::start_scan_when_scanning - configuring beam");
    configure_beam();
    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::start_scan_when_scanning - beam configured");

    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::start_scan_when_scanning - configuring scan");
    configure_scan();
    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::start_scan_when_scanning - scan configured");

    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::start_scan_when_scanning - starting scan");
    EXPECT_FALSE(handler->is_scanning());
    start_scan();
    EXPECT_TRUE(handler->is_scanning());

    // sleep for bit, want to wait so we can stop.
    usleep(microseconds_per_decisecond);

    try {
        start_scan();
    } catch (ska::pst::common::LmcServiceException& ex) {
        EXPECT_EQ(std::string(ex.what()), "DSP.DISK is already scanning.");
        EXPECT_EQ(ex.error_code(), ska::pst::lmc::ErrorCode::ALREADY_SCANNING);
        EXPECT_EQ(ex.status_code(), grpc::StatusCode::FAILED_PRECONDITION);
    }
}

TEST_F(DspDiskLmcServiceHandlerTest, start_scan_when_not_configured_should_throw_exception) // NOLINT
{
    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::start_scan_when_not_configured_should_throw_exception - configuring beam");
    configure_beam();
    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::start_scan_when_not_configured_should_throw_exception - beam configured");

    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::start_scan_when_not_configured_should_throw_exception - start scan");
    EXPECT_FALSE(handler->is_scan_configured());

    try {
        start_scan();
        FAIL() << " expected start_scan to throw exception due to scan not being configured.\n";
    } catch (ska::pst::common::LmcServiceException& ex) {
        EXPECT_EQ(std::string(ex.what()), "Not currently configured for DSP.DISK.");
        EXPECT_EQ(ex.error_code(), ska::pst::lmc::ErrorCode::NOT_CONFIGURED_FOR_SCAN);
        EXPECT_EQ(ex.status_code(), grpc::StatusCode::FAILED_PRECONDITION);
    }
}

TEST_F(DspDiskLmcServiceHandlerTest, start_scan_again_should_throw_exception) // NOLINT
{
    helper.write_data();

    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::start_scan_again_should_throw_exception - configuring beam");
    configure_beam();
    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::start_scan_again_should_throw_exception - beam configured");

    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::start_scan_again_should_throw_exception - configuring scan");
    configure_scan();
    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::start_scan_again_should_throw_exception - scan configured");

    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::start_scan_again_should_throw_exception - start scan");
    EXPECT_FALSE(handler->is_scanning());
    start_scan();
    EXPECT_TRUE(handler->is_scanning());
    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::start_scan_again_should_throw_exception - scanning");

    try {
        usleep(microseconds_per_decisecond);
        start_scan();
        FAIL() << " expected start_scan to throw exception due to already scanning.\n";
    } catch (ska::pst::common::LmcServiceException& ex) {
        EXPECT_EQ(std::string(ex.what()), "DSP.DISK is already scanning.");
        EXPECT_EQ(ex.error_code(), ska::pst::lmc::ErrorCode::ALREADY_SCANNING);
        EXPECT_EQ(ex.status_code(), grpc::StatusCode::FAILED_PRECONDITION);
    }
}

TEST_F(DspDiskLmcServiceHandlerTest, stop_scan_when_not_scanning_should_throw_exception) // NOLINT
{
    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::stop_scan_when_not_scanning_should_throw_exception - end scan");
    EXPECT_FALSE(handler->is_scanning());

    try {
        handler->stop_scan();
        FAIL() << " expected stop_scan to throw exception due to not currently scanning.\n";
    } catch (ska::pst::common::LmcServiceException& ex) {
        EXPECT_EQ(std::string(ex.what()), "Received stop_scan request when DSP.DISK is not scanning.");
        EXPECT_EQ(ex.error_code(), ska::pst::lmc::ErrorCode::NOT_SCANNING);
        EXPECT_EQ(ex.status_code(), grpc::StatusCode::FAILED_PRECONDITION);
    }
}

// test getting monitoring data
TEST_F(DspDiskLmcServiceHandlerTest, get_monitor_data) // NOLINT
{
    helper.write_data();

    // configure beam
    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::get_monitor_data - configuring beam");
    configure_beam();
    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::get_monitor_data - beam configured");

    // configure
    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::get_monitor_data - configuring scan");
    configure_scan();
    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::get_monitor_data - scan configured");

    // scan
    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::get_monitor_data - starting scan");
    start_scan();
    usleep(microseconds_per_decisecond);
    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::get_monitor_data - scanning");

    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::get_monitor_data - monitoring");
    ska::pst::lmc::MonitorData monitor_data;
    handler->get_monitor_data(&monitor_data);
    const auto &dsp_disk_stats = _dsp->get_disk_stats();

    EXPECT_TRUE(monitor_data.has_dsp_disk()); // NOLINT
    const auto &dsp_monitor_data = monitor_data.dsp_disk();

    EXPECT_EQ(dsp_monitor_data.disk_capacity(), dsp_disk_stats.capacity); // NOLINT
    EXPECT_EQ(dsp_monitor_data.disk_available_bytes(), dsp_disk_stats.available); // NOLINT
    EXPECT_EQ(dsp_monitor_data.bytes_written(), dsp_disk_stats.bytes_written); // NOLINT
    EXPECT_EQ(dsp_monitor_data.write_rate(), dsp_disk_stats.data_write_rate); // NOLINT

    // end scan
    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::get_monitor_data - ending scan");
    handler->stop_scan();
    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::get_monitor_data - scan ended");

    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::get_monitor_data - deconfiguring scan");
    handler->deconfigure_scan();
    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::get_monitor_data - scan deconfigured");

    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::get_monitor_data - deconfiguring beam");
    handler->deconfigure_beam();
    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::get_monitor_data - beam deconfigured");

}

TEST_F(DspDiskLmcServiceHandlerTest, get_monitor_data_when_not_scanning_should_throw_exception) // NOLINT
{
    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::get_monitor_data_when_not_scanning_should_throw_exception - end scan");
    EXPECT_FALSE(handler->is_scanning());

    try {
        ska::pst::lmc::MonitorData monitor_data;
        handler->get_monitor_data(&monitor_data);
        FAIL() << " expected get_monitor_data to throw exception due to not currently scanning.\n";
    } catch (ska::pst::common::LmcServiceException& ex) {
        EXPECT_EQ(std::string(ex.what()), "Received get_monitor_data request when DSP.DISK is not scanning.");
        EXPECT_EQ(ex.error_code(), ska::pst::lmc::ErrorCode::NOT_SCANNING);
        EXPECT_EQ(ex.status_code(), grpc::StatusCode::FAILED_PRECONDITION);
    }
}

TEST_F(DspDiskLmcServiceHandlerTest, get_env) // NOLINT
{
    ska::pst::lmc::GetEnvironmentResponse response;
    handler->get_env(&response);
    auto stats = _dsp->get_disk_stats();

    EXPECT_EQ(response.values().size(), 2);
    auto values = response.values();

    EXPECT_EQ(values.count("disk_capacity"), 1);
    EXPECT_TRUE(values["disk_capacity"].has_unsigned_int_value());
    EXPECT_EQ(values["disk_capacity"].unsigned_int_value(), stats.capacity);


    EXPECT_EQ(values.count("disk_available_bytes"), 1);
    EXPECT_TRUE(values["disk_available_bytes"].has_unsigned_int_value());
    EXPECT_EQ(values["disk_available_bytes"].unsigned_int_value(), stats.available);
}

TEST_F(DspDiskLmcServiceHandlerTest, go_to_runtime_error) // NOLINT
{
    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::go_to_runtime_error");

    try {
        throw std::runtime_error("this is a test error");
    } catch (...) {
        handler->go_to_runtime_error(std::current_exception());
    }

    ASSERT_EQ(ska::pst::common::RuntimeError, _dsp->get_state());
    ASSERT_EQ(ska::pst::common::RuntimeError, handler->get_application_manager_state());

    try {
        if (handler->get_application_manager_exception()) {
            std::rethrow_exception(handler->get_application_manager_exception());
        } else {
            // the exception should be set and not null
            ASSERT_FALSE(true);
        }
    } catch (const std::exception& exc) {
        ASSERT_EQ("this is a test error", std::string(exc.what()));
    }
}

TEST_F(DspDiskLmcServiceHandlerTest, reset_non_runtime_error_state) // NOLINT
{
    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::reset_non_runtime_error_state - configuring beam");
    configure_beam();
    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::reset_non_runtime_error_state - beam configured");

    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::reset_non_runtime_error_state - configuring scan");
    EXPECT_FALSE(handler->is_scan_configured());
    configure_scan();
    EXPECT_TRUE(handler->is_scan_configured());
    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::reset_non_runtime_error_state - scan configured");

    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::reset_non_runtime_error_state - resetting");
    handler->reset();
    EXPECT_TRUE(handler->is_scan_configured());
    EXPECT_TRUE(handler->is_beam_configured());

    EXPECT_EQ(ska::pst::common::State::ScanConfigured, handler->get_application_manager_state());
    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::reset_non_runtime_error_state - reset");
}

TEST_F(DspDiskLmcServiceHandlerTest, reset_from_runtime_error_state) // NOLINT
{
    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::reset_from_runtime_error_state - configuring beam");
    configure_beam();
    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::reset_from_runtime_error_state - beam configured");

    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::reset_from_runtime_error_state - configuring scan");
    EXPECT_FALSE(handler->is_scan_configured());
    configure_scan();
    EXPECT_TRUE(handler->is_scan_configured());
    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::reset_from_runtime_error_state - scan configured");

    try {
        throw std::runtime_error("force fault state");
    } catch (...) {
        handler->go_to_runtime_error(std::current_exception());
    }

    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::reset_from_runtime_error_state - resetting");
    handler->reset();
    EXPECT_FALSE(handler->is_scan_configured());
    EXPECT_FALSE(handler->is_beam_configured());

    EXPECT_EQ(ska::pst::common::State::Idle, handler->get_application_manager_state());
    SPDLOG_TRACE("DspDiskLmcServiceHandlerTest::reset_from_runtime_error_state - reset");
}

} // namespace ska::pst::dsp::test