
include_directories(../..)

add_executable(DspDiskLmcServiceHandlerTest src/DspDiskLmcServiceHandlerTest.cpp)
add_executable(DspDiskLmcServiceIntegrationTest src/DspDiskLmcServiceIntegrationTest.cpp)

set(TEST_LINK_LIBS 
    gtest_main
    ska_pst_dsp
    ska-pst-dsp-testutils
    ska-pst-dsp-disk-testutils
    ${SkaPstCommon_LIBRARIES}
)

target_link_libraries(DspDiskLmcServiceHandlerTest ${TEST_LINK_LIBS})
target_link_libraries(DspDiskLmcServiceIntegrationTest ${TEST_LINK_LIBS})

add_test(DspDiskLmcServiceHandlerTest DspDiskLmcServiceHandlerTest --test_data "${CMAKE_CURRENT_LIST_DIR}/../../tests/data")
add_test(DspDiskLmcServiceIntegrationTest DspDiskLmcServiceIntegrationTest --test_data "${CMAKE_CURRENT_LIST_DIR}/../../tests/data")
