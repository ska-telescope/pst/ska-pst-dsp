/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <cstddef>
#include <filesystem>
#include <mutex>
#include <inttypes.h>

#include "ska/pst/common/utils/AsciiHeader.h"
#include "ska/pst/common/utils/ValidationContext.h"
#include "ska/pst/common/utils/FileWriter.h"
#include "ska/pst/common/statemodel/ApplicationManager.h"

#include "ska/pst/smrb/DataBlockRead.h"
#include "ska/pst/dsp/disk/DiskMonitor.h"

#ifndef SKA_PST_DSP_DISK_StreamWriter_h
#define SKA_PST_DSP_DISK_StreamWriter_h

namespace ska::pst::dsp {

  /**
   * @brief The Stream Writer class adheres to the StateModel and provides the functionality
   * to write a data stream from a Shared Memory Ring Buffer to a series of files that are
   * stored on disk. It makes use of a DiskMonitor instance to record the data writing performance.
   *
   */
  class StreamWriter : public ska::pst::common::ApplicationManager {

    public:

      /**
       * @brief Construct a new StreamWriter object
       *
       * @param disk_monitor DiskMonitor instance to update with file writer statistics
       */
      StreamWriter(DiskMonitor& disk_monitor);

      /**
       * @brief Construct a new StreamWriter object that supports O_DIRECT file access.
       *
       * @param disk_monitor DiskMonitor instance to update with file writer statistics
       * @param use_o_direct Flag to enable the O_DIRECT option.
       */
      StreamWriter(DiskMonitor& disk_monitor, bool use_o_direct);

      /**
       * @brief Destroy the StreamWriter object
       *
       */
      ~StreamWriter();

      /**
       * @brief Validates Beam configuration. Specific validation errors must be set when throwing exceptions.
       *
       * @param config Beam configuration to validate
       * @param context A validation context where errors should be added.
       */
      void validate_configure_beam(const ska::pst::common::AsciiHeader& config, ska::pst::common::ValidationContext *context);

      /**
       * @brief Validates Scan configuration. Specific validation errors must be set when throwing exceptions.
       *
       * @param config Scan configuration to validate
       * @param context A validation context where errors should be added.
       */
      void validate_configure_scan(const ska::pst::common::AsciiHeader& config, ska::pst::common::ValidationContext *context);

      /**
       * @brief Validates StartScan configuration. Specific validation errors must be set when throwing exceptions.
       *
       * @param config StartScan configuration to validate
       *
       */
      void validate_start_scan(const ska::pst::common::AsciiHeader& config);

      /**
       * @brief Get the number of ring buffer elements to be written to each file
       *
       * @return uint64_t number of ring buffer elements to be written to each file
       */
      uint64_t get_bufs_written_per_file() { return bufs_written_per_file; };

      /**
       * @brief Set the number of full ring buffer elements to write to each file
       *
       * @param bufs_per_file number of full ring buffer elements to write to each file
       */
      void set_bufs_written_per_file(uint64_t bufs_per_file) { bufs_written_per_file = bufs_per_file; };

      /**
       * @brief Get the output path to which files will be written
       *
       * @return std::filesystem::path output path to which files will be written
       */
      std::filesystem::path get_output_path() { return output_path; };

      /**
       * @brief Mark the end of a scan by creating a file based on the state of the end of the perform_scan thread.
       * 
       * @param state state of the scan. Used as the file name of the marker file. Currently only writes scan_completed
       */
      void mark_scan(const std::string& state);

    private:

      //! for updating file write statistics
      DiskMonitor& disk_monitor;

      //! input data block to read from
      std::unique_ptr<ska::pst::smrb::DataBlockRead> db{nullptr};

      //! performs file write operations
      ska::pst::common::FileWriter file_writer;

      //! base directory to which scans will be witten
      std::filesystem::path recording_base_path;

      //! sub directory for this scan
      std::filesystem::path scan_path;

      //! sub-directory for this file writer stream
      std::filesystem::path stream_path;

      //! full output path for this file writer
      std::filesystem::path output_path;

      //! timeout to wait when attempting to connect to the DataBlockRead object.
      int timeout{0};

      //! the header parameters read from the data block
      ska::pst::common::AsciiHeader header;

      void perform_initialise();
      void perform_configure_beam();
      void perform_configure_scan();
      void perform_start_scan();
      void perform_scan();
      void perform_stop_scan();
      void perform_deconfigure_scan();
      void perform_deconfigure_beam();
      void perform_reset() { ; };
      void perform_terminate() { ; };

      /**
       * @brief Mapping between TELESCOPE and ska subsystem output path 
       *
       */
      std::map<std::string, std::string> subsystem_path_map {
        { "SKALow", "pst-low" },
        { "SKAMid", "pst-mid" },
      };

      //! rooth path where data products are written intended for file transfer
      std::string product_root_path = "product";

      //! the approximate number of seconds of data to be written to each data file
      double seconds_per_file{10};

      //! the total number of bytes to write to each output file in the data stream
      uint64_t bytes_written_per_file{0};

      //! the total number of full ring buffer elements written to the currently opened file
      uint64_t bufs_written_to_file{0};

      //! the total number of full ring buffer elements to write to each output file
      uint64_t bufs_written_per_file{0};

      //! data offset (in bytes) of data samples written to the file
      uint64_t obs_offset{0};

      //! size of the header ring buffer elements in bytes
      uint64_t header_bufsz{0};

      //! size of the data ring buffer element in bytes
      uint64_t data_bufsz{0};

      //! the timestamp corresponing to the first time sample in the data stream in YYYY-MM-DD-HH:MM:SS format
      std::string utc_start;

      //! the number of bytes per second of the data stream
      double bytes_per_second{0};

      //! the minimum atomic size of data that can be written to file
      uint64_t resolution{0};

      //! the current file number written to the data stream
      unsigned file_number{0};

  };

} // namespace ska::pst::dsp

#endif // SKA_PST_DSP_DISK_StreamWriter_h
