/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <string>

#include "ska/pst/common/statemodel/ApplicationManager.h"
#include "ska/pst/common/utils/ValidationContext.h"
#include "ska/pst/dsp/disk/StreamWriter.h"
#include "ska/pst/dsp/disk/DiskMonitor.h"

#ifndef __SKA_PST_DSP_DiskManager_h
#define __SKA_PST_DSP_DiskManager_h

namespace ska {
namespace pst {
namespace dsp {

  /**
   * @brief The DiskManager combines two instances of the StreamWriter and a DiskMonitor
   * to provide functionality that writes data and weights streams from Shared Memory Ring Buffers
   * to a series of files stored on the local disks. This class extended the StateModel, implementing
   * the required commands to step through state model common to PST C++ applications. The DiskMonitor
   * ensures the DiskManager can monitor the file system to which it is writing.
   *
   */
  class DiskManager : public ska::pst::common::ApplicationManager
  {

    public:

      /**
       * @brief Statistics of the disk
       *
       */
      typedef struct stats
      {
        //! Size of the recording disk in bytes
        uint64_t capacity;

        //! Bytes written to disk
        uint64_t bytes_written;

        //! Write rate to disk in bytes per second
        double data_write_rate;

        //! Expected write rate to disk in bytes per second
        double expected_data_write_rate;

        //! Available space on the recording disk in bytes
        uint64_t available;

      } stats_t;

      /**
       * @brief Default Construct a new Disk Manager object
       *
       */
      DiskManager(const std::string& recording_base_path, bool use_o_direct);

      /**
       * @brief Destroy the Disk Manager object
       *
       */
      ~DiskManager();

      /**
       * @brief Configure beam and scan as described by the configuration file
       *
       * @param config_file configuration file containing beam and scan configuration parameters
       */
      void configure_from_file(const std::string &config_file);

      /**
       * @brief Configure beam as described by the configuration parameters
       *
       * @param config beam configuration containing the recording path, data and weights key
       * @param context A validation context where errors should be added.
       */
      void validate_configure_beam(const ska::pst::common::AsciiHeader& config, ska::pst::common::ValidationContext *context);

      /**
       * @brief Configure scan as described by the configuration parameters
       *
       * @param config scan configuration containing the bytes_per_second and scan_len_max
       * @param context A validation context where errors should be added.
       */
      void validate_configure_scan(const ska::pst::common::AsciiHeader& config, ska::pst::common::ValidationContext *context);

      /**
       * @brief Start the scan using the provided configuration parameters
       *
       * @param config Scan runtime configuration parameters
       */
      void validate_start_scan(const ska::pst::common::AsciiHeader& config);

      /**
       * @brief Return the disk statistics of the Disk Monitor.
       */
      stats_t get_disk_stats();

      /**
       * @brief Get the beam configuration
       *
       * @return const ska::pst::common::AsciiHeader& header containing beam configuration
       * @throw std::runtime_exception if the beam is not configured
       */
      ska::pst::common::AsciiHeader& get_beam_configuration();

      /**
       * @brief Get the scan configuration.
       *
       * @return const ska::pst::common::AsciiHeader& header containing scan configuration
       * @throw std::runtime_exception if the scan is not configured
       */
      ska::pst::common::AsciiHeader& get_scan_configuration();

      /**
       * @brief Get the recording base path the Disk Manager is constructed with.
       *
       * @return std::string filesystem base path to where files will be written
       */
      std::string get_recording_base_path() { return recording_base_path; };

    private:

      // void enforce(bool required, const std::string& contextual_message) const;

      void perform_initialise();
      void perform_configure_beam();
      void perform_configure_scan();
      void perform_start_scan();
      void perform_scan();
      void perform_stop_scan();
      void perform_deconfigure_scan();
      void perform_deconfigure_beam();
      void perform_terminate() { ; };

      //! allocated configuration for beam of data stream and weights stream
      ska::pst::common::AsciiHeader data_beam_config;
      ska::pst::common::AsciiHeader weights_beam_config;

      //! shared pointer to the data ring buffer under management
      std::unique_ptr<StreamWriter> data_stream{nullptr};

      //! shared pointer to the weights ring buffer under management
      std::unique_ptr<StreamWriter> weights_stream{nullptr};

      //! disk monitor
      DiskMonitor disk_monitor;

      //! base directory to where files will be written
      std::string recording_base_path;

      //! use O_DIRECT I/O techniques in StreamWriters
      bool o_direct{false};
  };

} // dsp
} // pst
} // ska

#endif // __SKA_PST_DSP_DiskManager_h
