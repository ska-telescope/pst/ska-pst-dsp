
set(public_headers)

set(sources
    src/DiskManager.cpp
    src/DiskMonitor.cpp
    src/StreamWriter.cpp
)

set(private_headers
    DiskManager.h
    DiskMonitor.h
    StreamWriter.h
)

if (HAVE_PROTOBUF_gRPC_SKAPSTLMC)
    add_subdirectory(lmc)

    add_library(
        ska-pst-dsp-disk
        OBJECT
        ${sources}
        ${private_headers}
        ${public_headers}
        $<TARGET_OBJECTS:ska-pst-dsp-disk-lmc>
    )
else (HAVE_PROTOBUF_gRPC_SKAPSTLMC)
    add_library(
        ska-pst-dsp-disk
        OBJECT
        ${sources}
        ${private_headers}
        ${public_headers}
    )
endif(HAVE_PROTOBUF_gRPC_SKAPSTLMC)

target_include_directories(
    ska-pst-dsp-disk
    PUBLIC
    $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}/src>
    $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}/resources/spdlog/include>
    $<BUILD_INTERFACE:${PROJECT_BINARY_DIR}/src>
    $<INSTALL_INTERFACE:include>
    ${SkaPstCommon_INCLUDE_DIR}
    ${SkaPstSmrb_INCLUDE_DIR}
)

target_link_libraries(
    ska-pst-dsp-disk
    PUBLIC
    spdlog::spdlog
)

if (HAVE_PROTOBUF_gRPC_SKAPSTLMC)
  target_link_libraries(ska-pst-dsp-disk
    PUBLIC
      protobuf::libprotobuf
      gRPC::grpc
      gRPC::grpc++
      ${SkaPstLmc_LIBRARIES}
)
endif(HAVE_PROTOBUF_gRPC_SKAPSTLMC)

if (BUILD_TESTING)
    add_subdirectory(testutils)
    add_subdirectory(tests)
endif()

install(
    FILES
        ${public_headers}
    DESTINATION
        include/ska/pst/dsp
)
