/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <iostream>
#include <thread>
#include <spdlog/spdlog.h>

#include "ska/pst/common/utils/AsciiHeader.h"
#include "ska/pst/common/utils/Timer.h"
#include "ska/pst/dsp/disk/tests/DiskManagerTest.h"
#include "ska/pst/dsp/testutils/GtestMain.h"

auto main(int argc, char* argv[]) -> int
{
  return ska::pst::dsp::test::gtest_main(argc, argv);
}

namespace ska::pst::dsp::test {

DiskManagerTest::DiskManagerTest()
    : ::testing::Test()
{
}

void DiskManagerTest::SetUp()
{
  beam_config.load_from_file(test_data_file("beam_config.txt"));
  scan_config.load_from_file(test_data_file("scan_config.txt"));
  start_scan_config.load_from_file(test_data_file("start_scan_config.txt"));

  //disk_manager_config.load_from_file(test_data_file("DiskManager_config.txt"));

  data_scan_config.load_from_file(test_data_file("data_scan_config.txt"));
  weights_scan_config.load_from_file(test_data_file("weights_scan_config.txt"));

  data_header.load_from_file(test_data_file("data_header.txt"));
  weights_header.load_from_file(test_data_file("weights_header.txt"));

  setup_data_block();
}

void DiskManagerTest::setup_data_block()
{
  static constexpr uint64_t header_nbufs = 4;
  static constexpr uint64_t header_bufsz = 4096;
  static constexpr uint64_t data_nbufs = 8;
  static constexpr uint64_t weights_nbufs = 8;
  static constexpr uint64_t bufsz_factor = 16;
  static constexpr unsigned nreaders = 1;
  static constexpr int device = -1;
  uint64_t data_bufsz = data_header.get_uint64("RESOLUTION") * bufsz_factor;
  uint64_t weights_bufsz = weights_header.get_uint64("RESOLUTION") * bufsz_factor;
  SPDLOG_DEBUG("ska::pst::dsp::test::DiskManagerTest::setup_data_block data_bufsz={} weights_bufsz={}", data_bufsz, weights_bufsz);

  _dbc_data = std::make_unique<ska::pst::smrb::DataBlockCreate>(beam_config.get_val("DATA_KEY"));
  _dbc_data->create(header_nbufs, header_bufsz, data_nbufs, data_bufsz, nreaders, device);

  _writer_data = std::make_unique<ska::pst::smrb::DataBlockWrite>(beam_config.get_val("DATA_KEY"));
  _writer_data->connect(0);
  _writer_data->lock();

  _dbc_weights = std::make_unique<ska::pst::smrb::DataBlockCreate>(beam_config.get_val("WEIGHTS_KEY"));
  _dbc_weights->create(header_nbufs, header_bufsz, weights_nbufs, weights_bufsz, nreaders, device);

  _writer_weights = std::make_unique<ska::pst::smrb::DataBlockWrite>(beam_config.get_val("WEIGHTS_KEY"));
  _writer_weights->connect(0);
  _writer_weights->lock();

  data_to_write.resize(data_bufsz * 2);
  weights_to_write.resize(weights_bufsz * 2);

  _writer_data->write_config(data_scan_config.raw());
  _writer_weights->write_config(weights_scan_config.raw());

  _writer_data->write_header(data_header.raw());
  _writer_weights->write_header(weights_header.raw());
}

void DiskManagerTest::tear_down_data_block()
{
  if (_writer_data)
  {
    if (_writer_data->get_opened())
    {
      _writer_data->close();
    }
    if (_writer_data->get_locked())
    {
      _writer_data->unlock();
    }
    _writer_data->disconnect();
  }
  _writer_data = nullptr;

  if (_dbc_data)
  {
    _dbc_data->destroy();
  }
  _dbc_data = nullptr;
  if (_writer_weights)
  {
    if (_writer_weights->get_opened())
    {
      _writer_weights->close();
    }
    if (_writer_weights->get_locked())
    {
      _writer_weights->unlock();
    }
    _writer_weights->disconnect();
  }
  _writer_weights = nullptr;

  if (_dbc_weights)
  {
    _dbc_weights->destroy();
  }
  _dbc_weights = nullptr;
}

void DiskManagerTest::TearDown()
{
  tear_down_data_block();
}

TEST_F(DiskManagerTest, test_construct_delete) // NOLINT
{
  std::shared_ptr<DiskManager> dm = std::make_shared<DiskManager>("/tmp", false);
}

TEST_F(DiskManagerTest, test_configure_from_file) // NOLINT
{
  DiskManager dm("/tmp", false);
  EXPECT_FALSE(dm.is_beam_configured());
  SPDLOG_TRACE("ska::pst::dsp::test::DiskManagerTest::test_configure_from_file dm.is_beam_configured()={}",dm.is_beam_configured());
  EXPECT_FALSE(dm.is_scan_configured());
  SPDLOG_TRACE("ska::pst::dsp::test::DiskManagerTest::test_configure_from_file dm.is_scan_configured()={}",dm.is_scan_configured());
  dm.configure_from_file(test_data_file("DiskManager_config.txt"));
  EXPECT_TRUE(dm.is_beam_configured());
  EXPECT_TRUE(dm.is_scan_configured());
  EXPECT_TRUE(dm.is_scanning());

  _writer_data->open();
  _writer_data->write_data(&data_to_write[0], data_to_write.size());
  _writer_data->close();
  _writer_weights->open();
  _writer_weights->write_data(&weights_to_write[0], weights_to_write.size());
  _writer_weights->close();
}

TEST_F(DiskManagerTest, test_configure_beam) // NOLINT
{
  SPDLOG_TRACE("ska::pst::dsp::test::DiskManagerTest::test_configure_beam");
  DiskManager dm("/tmp", false);
  EXPECT_THROW(dm.get_beam_configuration(), std::runtime_error); // NOLINT
  SPDLOG_TRACE("ska::pst::dsp::test::DiskManagerTest::test_configure_beam dm.is_beam_configured()={}",dm.is_beam_configured());
  EXPECT_FALSE(dm.is_beam_configured());
  SPDLOG_TRACE("ska::pst::dsp::test::DiskManagerTest::test_configure_beam data_key={}",beam_config.get_val("DATA_KEY"));
  SPDLOG_TRACE("ska::pst::dsp::test::DiskManagerTest::test_configure_beam weights_key={}",beam_config.get_val("WEIGHTS_KEY"));
  dm.configure_beam(beam_config);
  EXPECT_TRUE(dm.is_beam_configured());
  EXPECT_NO_THROW(dm.get_beam_configuration()); // NOLINT
}

TEST_F(DiskManagerTest, test_configure_scan) // NOLINT
{
  SPDLOG_TRACE("ska::pst::dsp::test::DiskManagerTest::test_configure_scan");
  DiskManager dm("/tmp", false);
  SPDLOG_TRACE("ska::pst::dsp::test::DiskManagerTest::test_configure_scan dm.is_beam_configured()={}",dm.is_beam_configured());
  SPDLOG_TRACE("ska::pst::dsp::test::DiskManagerTest::test_configure_scan dm.is_scan_configured()={}",dm.is_scan_configured());
  EXPECT_FALSE(dm.is_beam_configured());
  dm.configure_beam(beam_config);
  EXPECT_THROW(dm.get_scan_configuration(), std::runtime_error); // NOLINT
  SPDLOG_TRACE("ska::pst::dsp::test::DiskManagerTest::test_configure_scan dm.is_beam_configured()={}",dm.is_beam_configured());
  EXPECT_TRUE(dm.is_beam_configured());

  EXPECT_FALSE(dm.is_scan_configured());
  SPDLOG_TRACE("ska::pst::dsp::test::DiskManagerTest::test_configure_scan dm.configure_scan()");
  dm.configure_scan(scan_config);
  SPDLOG_TRACE("ska::pst::dsp::test::DiskManagerTest::test_configure_scan dm.is_scan_configured()={}",dm.is_scan_configured());
  EXPECT_TRUE(dm.is_scan_configured());
  EXPECT_NO_THROW(dm.get_scan_configuration()); // NOLINT
}

TEST_F(DiskManagerTest, test_deconfigure_beam) // NOLINT
{
  SPDLOG_TRACE("ska::pst::dsp::test::DiskManagerTest::test_deconfigure_beam");
  DiskManager dm("/tmp", false);
  SPDLOG_TRACE("ska::pst::dsp::test::DiskManagerTest::test_deconfigure_beam dm.is_beam_configured()={}",dm.is_beam_configured());
  EXPECT_FALSE(dm.is_beam_configured());

  dm.configure_beam(beam_config);
  EXPECT_TRUE(dm.is_beam_configured());

  SPDLOG_TRACE("ska::pst::dsp::test::DiskManagerTest::test_deconfigure_beam dm.is_beam_configured()={}",dm.is_beam_configured());
  dm.deconfigure_beam();
  EXPECT_FALSE(dm.is_beam_configured());
}

TEST_F(DiskManagerTest, test_deconfigure_scan) // NOLINT
{
  SPDLOG_TRACE("ska::pst::dsp::test::DiskManagerTest::test_deconfigure_scan");
  DiskManager dm("/tmp", false);
  SPDLOG_TRACE("ska::pst::dsp::test::DiskManagerTest::test_deconfigure_scan dm.is_beam_configured()={}",dm.is_beam_configured());
  EXPECT_FALSE(dm.is_beam_configured());

  dm.configure_beam(beam_config);
  EXPECT_TRUE(dm.is_beam_configured());

  dm.configure_scan(scan_config);
  EXPECT_TRUE(dm.is_scan_configured());

  dm.deconfigure_scan();
  SPDLOG_TRACE("ska::pst::dsp::test::DiskManagerTest::test_deconfigure_scan dm.is_scan_configured()={}",dm.is_scan_configured());
  EXPECT_FALSE(dm.is_scan_configured());

  dm.deconfigure_beam();
  EXPECT_FALSE(dm.is_beam_configured());
}

TEST_F(DiskManagerTest, test_start_stop_scan) // NOLINT
{
  SPDLOG_TRACE("ska::pst::dsp::test::DiskManagerTest::test_start_stop_scan");

  _writer_data->open();
  //_writer_data->write_header(config.raw());
  _writer_data->write_data(&data_to_write[0], data_to_write.size());
  _writer_data->close();
  _writer_weights->open();
  //_writer_weights->write_header(config.raw());
  _writer_weights->write_data(&weights_to_write[0], weights_to_write.size());
  _writer_weights->close();
  DiskManager dm("/tmp", false);
  EXPECT_FALSE(dm.is_beam_configured());
  dm.configure_beam(beam_config);
  EXPECT_TRUE(dm.is_beam_configured());

  EXPECT_FALSE(dm.is_scan_configured());
  dm.configure_scan(scan_config);
  EXPECT_TRUE(dm.is_scan_configured());

  SPDLOG_TRACE("ska::pst::dsp::test::DiskManagerTest::test_start_stop_scan dm.start_scan()");
  dm.start_scan(start_scan_config);
  usleep(microseconds_per_decisecond);
  dm.get_disk_stats();
  dm.stop_scan();

  dm.deconfigure_scan();
  EXPECT_FALSE(dm.is_scan_configured());

  dm.deconfigure_beam();
  EXPECT_FALSE(dm.is_beam_configured());
}

TEST_F(DiskManagerTest, test_multiple_scans) // NOLINT
{
  SPDLOG_TRACE("ska::pst::dsp::test::DiskManagerTest::test_multiple_scans");

  auto scan_id = start_scan_config.get_int32("SCAN_ID");
  DiskManager dm("/tmp", false);
  for (auto i = 0; i < 2; i++) {
    // update the scan ID in the configs
    data_scan_config.set("SCAN_ID", scan_id + i);
    weights_scan_config.set("SCAN_ID", scan_id + i);
    start_scan_config.set("SCAN_ID", scan_id + i);
    data_header.set("SCAN_ID", scan_id + i);
    weights_header.set("SCAN_ID", scan_id + i);

    if (i > 0) 
    {
      // Note that setup_data_block is called by ::Setup and therefore already done for the first loop
      setup_data_block();
    }

    _writer_data->open();
    _writer_data->write_data(&data_to_write[0], data_to_write.size());
    _writer_data->close();
    _writer_weights->open();
    _writer_weights->write_data(&weights_to_write[0], weights_to_write.size());
    _writer_weights->close();

    EXPECT_FALSE(dm.is_beam_configured());
    dm.configure_beam(beam_config);
    EXPECT_TRUE(dm.is_beam_configured());

    EXPECT_FALSE(dm.is_scan_configured());
    dm.configure_scan(scan_config);
    EXPECT_TRUE(dm.is_scan_configured());

    SPDLOG_TRACE("ska::pst::dsp::test::DiskManagerTest::test_multiple_scans dm.start_scan()");
    SPDLOG_INFO("Starting scan");
    dm.start_scan(start_scan_config);
    SPDLOG_INFO("Scan started sleeping");
    usleep(microseconds_per_decisecond);
    SPDLOG_INFO("Getting disk stats");
    dm.get_disk_stats();
    dm.stop_scan();

    dm.deconfigure_scan();
    EXPECT_FALSE(dm.is_scan_configured());

    dm.deconfigure_beam();
    EXPECT_FALSE(dm.is_beam_configured());

    tear_down_data_block();
  }
}

TEST_F(DiskManagerTest, test_multiple_scans_one_config) // NOLINT
{
  SPDLOG_TRACE("ska::pst::dsp::test::DiskManagerTest::test_multiple_scans_one_config");

  auto scan_id = start_scan_config.get_int32("SCAN_ID");
  DiskManager dm("/tmp", false);

  EXPECT_FALSE(dm.is_beam_configured());
  dm.configure_beam(beam_config);
  EXPECT_TRUE(dm.is_beam_configured());

  EXPECT_FALSE(dm.is_scan_configured());
  dm.configure_scan(scan_config);
  EXPECT_TRUE(dm.is_scan_configured());

  for (auto i = 0; i < 3; i++)
  {
    // update the scan ID in the configs
    data_scan_config.set("SCAN_ID", scan_id + i);
    weights_scan_config.set("SCAN_ID", scan_id + i);
    start_scan_config.set("SCAN_ID", scan_id + i);
    data_header.set("SCAN_ID", scan_id + i);
    weights_header.set("SCAN_ID", scan_id + i);

    if (i > 0) 
    {
      /* Note that these are teh last two lines of setup_data_block,
        which is called by ::Setup and is therefore already done for the first loop */
      _writer_data->write_header(data_header.raw());
      _writer_weights->write_header(weights_header.raw());
    }

    _writer_data->open();
    _writer_data->write_data(&data_to_write[0], data_to_write.size());
    _writer_data->close();
    _writer_weights->open();
    _writer_weights->write_data(&weights_to_write[0], weights_to_write.size());
    _writer_weights->close();

    SPDLOG_TRACE("ska::pst::dsp::test::DiskManagerTest::test_multiple_scans_one_config dm.start_scan()");
    SPDLOG_INFO("Starting scan");
    dm.start_scan(start_scan_config);
    SPDLOG_INFO("Scan started sleeping");
    usleep(microseconds_per_decisecond);
    SPDLOG_INFO("Getting disk stats");
    dm.get_disk_stats();
    dm.stop_scan();
  }

  dm.deconfigure_scan();
  EXPECT_FALSE(dm.is_scan_configured());

  dm.deconfigure_beam();
  EXPECT_FALSE(dm.is_beam_configured());

  tear_down_data_block();
}

TEST_F(DiskManagerTest, test_get_configuration) // NOLINT
{
  SPDLOG_TRACE("ska::pst::dsp::test::DiskManagerTest::test_get_configuration");
  DiskManager dm("/tmp", false);
  dm.configure_beam(beam_config);

  EXPECT_EQ(beam_config.get_val("DATA_KEY"), dm.get_beam_configuration().get_val("DATA_KEY"));
  EXPECT_EQ(beam_config.get_val("WEIGHTS_KEY"), dm.get_beam_configuration().get_val("WEIGHTS_KEY"));

  dm.configure_scan(scan_config);
  EXPECT_EQ(scan_config.get_double("BYTES_PER_SECOND"), dm.get_scan_configuration().get_double("BYTES_PER_SECOND"));
  EXPECT_EQ(scan_config.get_double("SCANLEN_MAX"), dm.get_scan_configuration().get_double("SCANLEN_MAX"));
}

} // namespace ska::pst::dsp::test