/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <spdlog/spdlog.h>

#include "ska/pst/common/statemodel/StateModelException.h"
#include "ska/pst/dsp/testutils/GtestMain.h"
#include "ska/pst/dsp/disk/tests/DiskMonitorTest.h"

auto main(int argc, char* argv[]) -> int
{
  return ska::pst::dsp::test::gtest_main(argc, argv);
}

namespace ska::pst::dsp::test {

DiskMonitorTest::DiskMonitorTest()
    : ::testing::Test()
{
}

void DiskMonitorTest::SetUp()
{
  recording_path = "/tmp";
  beam_config.load_from_file(test_data_file("beam_config.txt"));
  scan_config.load_from_file(test_data_file("data_scan_config.txt"));
  start_scan_config.load_from_file(test_data_file("start_scan_config.txt"));
}

void DiskMonitorTest::TearDown()
{
}

TEST_F(DiskMonitorTest, test_default_constructor) // NOLINT
{
  try {
    DiskMonitor mon(recording_path);
  } catch (std::exception &exc) {
    SPDLOG_WARN("ska::pst::dsp::test::DiskMonitorTest exc={}", exc.what());
  }
}

TEST_F(DiskMonitorTest, test_contructor_bad_filesystem) // NOLINT
{
  std::string bad_recording_path = "/does/not/exist";
  EXPECT_THROW(DiskMonitor mon(bad_recording_path), std::exception); // NOLINT
}

TEST_F(DiskMonitorTest, test_destructor_cleanup) // NOLINT
{
  DiskMonitor mon(recording_path);
  EXPECT_TRUE(mon.is_idle());
  mon.configure_beam(beam_config);
  mon.configure_scan(scan_config);
  mon.start_scan(start_scan_config);
}

TEST_F(DiskMonitorTest, test_configure_beam) // NOLINT
{
  DiskMonitor mon(recording_path);
  EXPECT_THROW(mon.deconfigure_beam(), ska::pst::common::pst_state_transition_error); // NOLINT
  EXPECT_FALSE(mon.is_beam_configured());
  mon.configure_beam(beam_config);
  EXPECT_TRUE(mon.is_beam_configured());
  mon.deconfigure_beam();
  EXPECT_FALSE(mon.is_beam_configured());
}

TEST_F(DiskMonitorTest, test_configure) // NOLINT
{
  DiskMonitor mon(recording_path);
  EXPECT_THROW(mon.configure_scan(scan_config), ska::pst::common::pst_state_transition_error); // NOLINT
  EXPECT_FALSE(mon.is_scan_configured());
  mon.configure_beam(beam_config);
  EXPECT_FALSE(mon.is_scan_configured());
  mon.configure_scan(scan_config);
  EXPECT_TRUE(mon.is_scan_configured());
  mon.deconfigure_scan();
  EXPECT_FALSE(mon.is_scan_configured());
  mon.deconfigure_beam();
  EXPECT_FALSE(mon.is_scan_configured());
}

TEST_F(DiskMonitorTest, test_scanning) // NOLINT
{
  static constexpr uint32_t monitor_sleep = 1100000;
  DiskMonitor mon(recording_path);
  EXPECT_THROW(mon.start_scan(start_scan_config), ska::pst::common::pst_state_transition_error); // NOLINT
  EXPECT_FALSE(mon.is_scanning());
  mon.configure_beam(beam_config);

  EXPECT_THROW(mon.start_scan(start_scan_config), ska::pst::common::pst_state_transition_error); // NOLINT
  EXPECT_FALSE(mon.is_scanning());
  mon.configure_scan(scan_config);

  EXPECT_FALSE(mon.is_scanning());
  mon.start_scan(start_scan_config);
  EXPECT_TRUE(mon.is_scanning());
  EXPECT_THROW(mon.start_scan(start_scan_config), ska::pst::common::pst_state_transition_error); // NOLINT

  ASSERT_GT(mon.get_disk_capacity(), 0);
  ASSERT_GT(mon.get_disk_available(), 0);
  ASSERT_GT(mon.get_recording_time_available(), 0);

  ASSERT_GT(mon.get_disk_capacity(), 0);
  ASSERT_GT(mon.get_disk_available(), 0);
  ASSERT_GT(mon.get_recording_time_available(), 0);

  mon.stop_scan();
  EXPECT_FALSE(mon.is_scanning());
  mon.deconfigure_scan();
  EXPECT_FALSE(mon.is_scanning());
  mon.deconfigure_beam();
  EXPECT_FALSE(mon.is_scanning());
}

TEST_F(DiskMonitorTest, test_increment_bytes) // NOLINT
{
  static constexpr uint64_t bytes_to_write = 1024;
  DiskMonitor mon(recording_path);
  mon.configure_beam(beam_config);
  mon.configure_scan(scan_config);
  EXPECT_THROW(mon.increment_bytes(bytes_to_write), std::runtime_error); // NOLINT
  mon.start_scan(start_scan_config);
  mon.increment_bytes(bytes_to_write);
  EXPECT_EQ(mon.get_bytes_written(), bytes_to_write);
  mon.increment_bytes(bytes_to_write);
  EXPECT_EQ(mon.get_bytes_written(), bytes_to_write + bytes_to_write);
  mon.stop_scan();
  mon.deconfigure_scan();
  mon.deconfigure_beam();
}

TEST_F(DiskMonitorTest, test_write_rate) // NOLINT
{
  static constexpr uint32_t writer_sleep = 1100000;
  DiskMonitor mon(recording_path);

  mon.configure_beam(beam_config);
  mon.configure_scan(scan_config);
  mon.start_scan(start_scan_config);

  double rate = scan_config.get_double("BYTES_PER_SECOND");

  ASSERT_EQ(mon.get_bytes_written(), 0);
  ASSERT_EQ(mon.get_data_write_rate(), 0);
  ASSERT_EQ(mon.get_expected_data_write_rate(), rate);

  static constexpr uint64_t nbytes = 10240;
  mon.increment_bytes(nbytes);

  usleep(writer_sleep);

  ASSERT_EQ(mon.get_bytes_written(), nbytes);
  ASSERT_GE(mon.get_data_write_rate(), nbytes * 0.50);
  ASSERT_LE(mon.get_data_write_rate(), nbytes * 2.00);
  ASSERT_EQ(mon.get_expected_data_write_rate(), rate);

  mon.stop_scan();
  mon.deconfigure_scan();
  mon.deconfigure_beam();
}

TEST_F(DiskMonitorTest, test_too_long_scan) // NOLINT
{
  DiskMonitor mon(recording_path);
  mon.configure_beam(beam_config);
  ska::pst::common::AsciiHeader bad_scan_config(scan_config);
  int32_t max_scanlen = INT32_MAX;
  bad_scan_config.set("SCANLEN_MAX", max_scanlen);
  EXPECT_THROW(mon.configure_scan(bad_scan_config), std::exception); // NOLINT
  mon.deconfigure_beam();
}

} // namespace ska::pst::dsp::test