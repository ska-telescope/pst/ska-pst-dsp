/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <spdlog/spdlog.h>
#include <filesystem>
#include <vector>

#include "ska/pst/common/statemodel/StateModelException.h"
#include "ska/pst/dsp/testutils/GtestMain.h"
#include "ska/pst/dsp/disk/tests/StreamWriterTest.h"

auto main(int argc, char* argv[]) -> int
{
  return ska::pst::dsp::test::gtest_main(argc, argv);
}

namespace ska::pst::dsp::test {

StreamWriterTest::StreamWriterTest()
    : ::testing::Test(), _monitor("/tmp")
{
}

void StreamWriterTest::SetUp()
{
  beam_config.load_from_file(test_data_file("beam_config.txt"));
  scan_config.load_from_file(test_data_file("data_scan_config.txt"));
  start_scan_config.load_from_file(test_data_file("start_scan_config.txt"));

  header.load_from_file(test_data_file("data_header.txt"));
  beam_config.set_val("KEY", "dada");
  beam_config.set_val("RECORDING_BASE_PATH", "/tmp");
  beam_config.set_val("RECORDING_SUFFIX", "data");

  static constexpr uint64_t header_nbufs = 4;
  static constexpr uint64_t header_bufsz = 4096;
  static constexpr uint64_t data_nbufs = 8;
  static constexpr uint64_t bufsz_factor = 16;
  static constexpr unsigned nreaders = 1;
  static constexpr int device = -1;
  uint64_t data_bufsz = header.get_uint64(std::string("RESOLUTION")) * bufsz_factor;

  _dbc = std::make_unique<ska::pst::smrb::DataBlockCreate>("dada");
  _dbc->create(header_nbufs, header_bufsz, data_nbufs, data_bufsz, nreaders, device);

  _writer = std::make_unique<ska::pst::smrb::DataBlockWrite>("dada");
  _writer->connect(0);
  _writer->lock();
  _writer->write_config(scan_config.raw());
  _writer->write_header(header.raw());

  data_to_write.resize(data_bufsz * 2);
}

void StreamWriterTest::TearDown()
{
  if (_writer)
  {
    if (_writer->get_opened())
    {
      _writer->close();
    }
    if (_writer->get_locked())
    {
      _writer->unlock();
    }
    _writer->disconnect();
  }
  _writer = nullptr;

  if (_dbc)
  {
    _dbc->destroy();
  }
  _dbc = nullptr;
}

void StreamWriterTest::write_bytes_to_writer(uint64_t bytes_to_write)
{
  std::vector<char> data(bytes_to_write);
  _writer->write_data(&data[0], data.size());
}

TEST_F(StreamWriterTest, test_default_constructor) // NOLINT
{
  StreamWriter sw(_monitor);
}

TEST_F(StreamWriterTest, test_default_constructor_o_direct) // NOLINT
{
  static constexpr bool use_o_direct = false;
  StreamWriter sw(_monitor, use_o_direct);
}

TEST_F(StreamWriterTest, test_destructor_cleanup) // NOLINT
{
  static constexpr double bytes_per_second = 1024;
  static constexpr double scan_len_max = 1;

  SPDLOG_TRACE("ska::pst::dsp::test::StreamWriterTest::test_destructor_cleanup data_to_write.size()={}", data_to_write.size());
  _writer->open();
  _writer->write_data(&data_to_write[0], data_to_write.size());
  _writer->close();

  StreamWriter sw(_monitor);
  SPDLOG_TRACE("ska::pst::dsp::test::StreamWriterTest::test_destructor_cleanup configure_beam()");
  _monitor.configure_beam(beam_config);
  sw.configure_beam(beam_config);

  SPDLOG_TRACE("ska::pst::dsp::test::StreamWriterTest::test_destructor_cleanup configure_scan()");
  _monitor.configure_scan(scan_config);
  sw.configure_scan(scan_config);

  SPDLOG_TRACE("ska::pst::dsp::test::StreamWriterTest::test_destructor_cleanup start_scan()");
  _monitor.start_scan(start_scan_config);
  sw.start_scan(start_scan_config);
  SPDLOG_TRACE("ska::pst::dsp::test::StreamWriterTest::test_destructor_cleanup usleep({})", microseconds_per_decisecond);
  usleep(microseconds_per_decisecond);
  SPDLOG_TRACE("ska::pst::dsp::test::StreamWriterTest::test_destructor_cleanup allowing class to go out of scope");
}

TEST_F(StreamWriterTest, test_configure_beam) // NOLINT
{
  StreamWriter sw(_monitor);
  EXPECT_THROW(sw.deconfigure_beam(), ska::pst::common::pst_state_transition_error); // NOLINT
  EXPECT_FALSE(sw.is_beam_configured());
  sw.configure_beam(beam_config);
  EXPECT_TRUE(sw.is_beam_configured());
  sw.deconfigure_beam();
  EXPECT_FALSE(sw.is_beam_configured());
}

TEST_F(StreamWriterTest, test_o_direct_with_bad_datablock_size) // NOLINT
{
  ska::pst::smrb::DataBlockCreate _bad_dbc("eada");
  static constexpr uint64_t header_nbufs = 8;
  static constexpr uint64_t good_header_bufsz = 4096;
  static constexpr uint64_t bad_header_bufsz = 4097;
  static constexpr uint64_t data_nbufs = 8;
  static constexpr uint64_t good_data_bufsz = 524288;
  static constexpr uint64_t bad_data_bufsz = 524289;
  static constexpr unsigned nreaders = 1;
  static constexpr int device = -1;
  _bad_dbc.create(header_nbufs, bad_header_bufsz, data_nbufs, good_data_bufsz, nreaders, device);

  ska::pst::smrb::DataBlockWrite _bad_writer("eada");
  _bad_writer.connect(0);
  _bad_writer.lock();

  ska::pst::common::AsciiHeader bad_config;
  bad_config.clone(beam_config);
  bad_config.set_val("KEY", "eada");

  StreamWriter sw(_monitor, true);

  _monitor.configure_beam(beam_config);
  SPDLOG_TRACE("ska::pst::dsp::test testing header data block bufsz");
  EXPECT_THROW(sw.configure_beam(bad_config), std::exception); // NOLINT
  sw.reset();

  _bad_dbc.destroy();
  _bad_dbc.create(header_nbufs, good_header_bufsz, data_nbufs, bad_data_bufsz, nreaders, device);

  SPDLOG_TRACE("ska::pst::dsp::test testing bad data block bufsz");
  EXPECT_THROW(sw.configure_beam(bad_config), std::exception); // NOLINT
  sw.reset();
}

TEST_F(StreamWriterTest, test_o_direct_with_shorted_block) // NOLINT
{
  static constexpr double bytes_per_second = 1024;
  static constexpr double scan_len_max = 1;

  SPDLOG_TRACE("ska::pst::dsp::test::StreamWriterTest::test_o_direct_with_shorted_block data_to_write.size()={}", data_to_write.size());
  _writer->open();
  _writer->write_data(&data_to_write[0], data_to_write.size() - 1);
  _writer->close();

  StreamWriter sw(_monitor, true);
  SPDLOG_TRACE("ska::pst::dsp::test::StreamWriterTest::test_o_direct_with_shorted_block configure_beam()");
  _monitor.configure_beam(beam_config);
  sw.configure_beam(beam_config);

  SPDLOG_TRACE("ska::pst::dsp::test::StreamWriterTest::test_o_direct_with_shorted_block configure_scan()");
  _monitor.configure_scan(scan_config);
  sw.configure_scan(scan_config);

  SPDLOG_TRACE("ska::pst::dsp::test::StreamWriterTest::test_o_direct_with_shorted_block start_scan()");
  _monitor.start_scan(start_scan_config);
  sw.start_scan(start_scan_config);
  SPDLOG_TRACE("ska::pst::dsp::test::StreamWriterTest::test_o_direct_with_shorted_block usleep({})", microseconds_per_decisecond);
  usleep(microseconds_per_decisecond);

  _monitor.stop_scan();
  sw.stop_scan();

  _monitor.deconfigure_scan();
  sw.deconfigure_scan();

  _monitor.deconfigure_beam();
  sw.deconfigure_beam();
}

TEST_F(StreamWriterTest, test_recording_multiple_files) // NOLINT
{
  StreamWriter sw(_monitor);

  static constexpr double scan_len_max = 15;
  double bytes_per_second = header.get_double("BYTES_PER_SECOND");
  auto bytes_to_write = uint64_t(floor(bytes_per_second * scan_len_max));

  _writer->open();
  std::thread _writer_thread = std::thread(&StreamWriterTest::write_bytes_to_writer, this, bytes_to_write);

  _monitor.configure_beam(beam_config);
  sw.configure_beam(beam_config);
  _monitor.configure_scan(scan_config);
  sw.configure_scan(scan_config);

  _monitor.start_scan(start_scan_config);
  sw.start_scan(start_scan_config);

  _writer_thread.join();
  _writer->close();

  SPDLOG_TRACE("ska::pst::dsp::test::StreamWriterTest::test_recording_multiple_files sw.stop_scan()");
  sw.stop_scan();
  _monitor.stop_scan();
  sw.deconfigure_scan();
  _monitor.deconfigure_scan();
  sw.deconfigure_beam();
  _monitor.deconfigure_beam();

  // check 2 files have been written to the file-system
  uint64_t obs_offset = 0;
  uint64_t file_number = 0;
  uint64_t resolution = std::max(header.get_uint64("RESOLUTION"), _writer->get_data_bufsz());
  std::string utc_start = header.get_val("UTC_START");

  static constexpr double file_duration = 10;
  auto bytes_per_file = uint64_t(floor(bytes_per_second * file_duration));
  uint64_t remainder = bytes_per_file % resolution;
  if (remainder > 0)
  {
    bytes_per_file += (resolution - remainder);
  }

  std::filesystem::path output_path = sw.get_output_path();

  std::filesystem::path file1 = output_path / ska::pst::common::FileWriter::get_filename(utc_start, obs_offset, file_number);

  file_number++;
  obs_offset += bytes_per_file;

  std::filesystem::path file2 = output_path / ska::pst::common::FileWriter::get_filename(utc_start, obs_offset, file_number);

  SPDLOG_DEBUG("ska::pst::dsp::test::StreamWriterTest::test_recording_multiple_files file1={} file2={}", file1.generic_string(), file2.generic_string());
  ASSERT_TRUE(std::filesystem::exists(file1));
  ASSERT_TRUE(std::filesystem::exists(file2));
}

TEST_F(StreamWriterTest, test_stop_scan_marker_existence) // NOLINT
{
  StreamWriter sw(_monitor);

  static constexpr double scan_len_max = 15;
  double bytes_per_second = header.get_double("BYTES_PER_SECOND");
  auto bytes_to_write = uint64_t(floor(bytes_per_second * scan_len_max));

  SPDLOG_INFO("ska::pst::dsp::test::StreamWriterTest::test_stop_scan_marker_existence beam_config={}", beam_config.raw());
  _writer->open();
  std::thread _writer_thread = std::thread(&StreamWriterTest::write_bytes_to_writer, this, bytes_to_write);

  std::string recording_base_path = beam_config.get_val("RECORDING_BASE_PATH");

  // append product path and execution block id
  std::string eb_id =  start_scan_config.get_val("EB_ID");

  // append subsystem id
  std::string subsystem_id =  subsystem_path_map[scan_config.get_val("TELESCOPE")];

  // append scan id
  std::string scan_id = start_scan_config.get_val("SCAN_ID");

  std::filesystem::path stop_scan_marker = std::filesystem::path(recording_base_path) / "product" / eb_id / subsystem_id / scan_id / marker_file;
  SPDLOG_TRACE("ska::pst::dsp::test::StreamWriterTest::test_stop_scan_marker_existence stop_scan_marker={}",stop_scan_marker.generic_string());

  _monitor.configure_beam(beam_config);
  sw.configure_beam(beam_config);
  _monitor.configure_scan(scan_config);
  sw.configure_scan(scan_config);

  _monitor.start_scan(start_scan_config);
  sw.start_scan(start_scan_config);

  _writer_thread.join();
  _writer->close();

  SPDLOG_TRACE("ska::pst::dsp::test::StreamWriterTest::test_recording_multiple_files sw.stop_scan()");
  sw.stop_scan();
  _monitor.stop_scan();
  ASSERT_TRUE(std::filesystem::exists(stop_scan_marker));

  sw.deconfigure_scan();
  _monitor.deconfigure_scan();
  sw.deconfigure_beam();
  _monitor.deconfigure_beam();
}

} // namespace ska::pst::dsp::test
