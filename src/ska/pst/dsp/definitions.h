/*
 * Copyright 2022 Square Kilometre Array Observatory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SKA_PST_DSP_definitions_h
#define SKA_PST_DSP_definitions_h

namespace ska::pst::dsp
{
  //! numer of bytes per 32-bit float
  static constexpr uint32_t bits_per_float = 32;

  //! number of micro seconds in a second
  static constexpr double microseconds_per_second = 1e6;

  //! number of micro seconds in a tenth of a second second
  static constexpr double microseconds_per_decisecond = 1e5;

  //! number of seconds per microsecond
  static constexpr double seconds_per_microsecond = 0.000001;

  //! number of deciseconds in a second
  static constexpr uint32_t deciseconds_per_second = 10;

  //! percentiles
  static constexpr double centiles = 100;

  //! number of bits in a byte
  static constexpr uint32_t bits_per_byte = 8;

  //! number of bits per relative weight
  static constexpr uint32_t weights_nbit = 16;

} // namespace ska::pst::dsp

#endif // SKA_PST_DSP_defintions_h