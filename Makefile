## The following should be standard includes
# include core makefile targets for release management
include .make/base.mk

# include oci makefile targets for oci management
include .make/oci.mk

# include helm make support
include .make/helm.mk

# include k8s make support
include .make/k8s.mk

# TEST: common pst makefile library
-include .pst/base.mk

# include your own private variables for custom deployment configuration
-include PrivateRules.mak

DEV_IMAGE	?=registry.gitlab.com/ska-telescope/pst/ska-pst-smrb/ska-pst-smrb-builder
DEV_TAG		?=0.10.6
PROJECT=ska-pst-dsp
PROCESSOR_COUNT=${nproc}
OCI_IMAGE_BUILD_CONTEXT=$(PWD)
PIV_COMMAND=/opt/bin/ska_pst_dsp_info

# Variables populated for local development and oci build.
# 	Overriden by CI variables. See .gitlab-cy.yml#L7
PST_SMRB_OCI_REGISTRY	?=registry.gitlab.com/ska-telescope/pst/ska-pst-smrb
SMRB_RUNTIME_IMAGE		?=${PST_SMRB_OCI_REGISTRY}/ska-pst-smrb
DSP_BUILDER_IMAGE		?=${PST_SMRB_OCI_REGISTRY}/ska-pst-smrb-builder
DSP_RUNTIME_IMAGE		?=ubuntu:22.04
# This should be initialised in PrivateRules.mak
PST_SMRB_OCI_COMMON_TAG		?=${DEV_TAG}
OCI_BUILD_ADDITIONAL_ARGS	=--build-arg SMRB_RUNTIME_IMAGE=${SMRB_RUNTIME_IMAGE}:${PST_SMRB_OCI_COMMON_TAG} --build-arg DSP_BUILDER_IMAGE=${DSP_BUILDER_IMAGE}:${PST_SMRB_OCI_COMMON_TAG} --build-arg DSP_RUNTIME_IMAGE=${DSP_RUNTIME_IMAGE}

# Extend pipeline machinery targets
.PHONY: docs-pre-build
docs-pre-build:
	@rm -rf docs/build/*
	apt-get update -y
	apt-get install -y doxygen
	pip3 install -r docs/requirements.txt

_VENV=.venv
_REQUIREMENTS=resources/k8s-bdd/requirements/k8s-bdd.python.txt
.PHONY: local-init-venv local-test-k8slib
PROJECT=ska-pst-dsp
local-helm-build:
	helm dependency build charts/$(PROJECT)
local-init-venv:
	$(call venv_exec,$(_VENV),pip install -r $(_REQUIREMENTS))

BDD_K8S_CMD=pytest -v -spP -o bdd_features_base_dir=$(PWD)/tests/bdd/ --junitxml=./build/k8sbdd.xml resources/k8s-bdd/src/bdd/k8stest.py
local-test-bdd:
	$(call venv_exec,$(_VENV),$(BDD_K8S_CMD))

# VENV FUNCTIONS
define venv_exec
	$(if [ ! -f "$($(1)/bin/activate)" ], python3 -m venv $(1))
	( \
    	source $(1)/bin/activate; \
    	$(2) \
	)
endef
