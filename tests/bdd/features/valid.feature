Feature: E2E k8s bdd deployment test for ska-pst-dsp
Scenario Outline: Generate k8s manifest
    Given path <chartpath> contains helm chart <chart>
    And path <overridepath> contains override file <override>
    When helm chart <chart> in <chartpath> is rendered using <override> in <overridepath> to <buildpath>
    Then helm chart <chart> rendered manifests exists in <buildpath>

    Examples:
    | chartpath | chart | overridepath | override | buildpath |
    | charts | ska-pst-dsp | tests/integration/k8s-test | test.k8srunner.yaml | build |


Scenario Outline: Deploy k8s manifest
    Given manifests exists in <buildpath>
    And environment variable for k8s namespace <envvar> is defined
    When manifests in <buildpath> gets deployed
    Then kubernetes resources are created in the namespace defined in the environment variable <envvar>

    Examples:
    | buildpath | envvar |
    | build | KUBE_NAMESPACE |


Scenario Outline: Deployed k8s pods are in its expected state
    Given manifests exists in <buildpath>
    And kubernetes resources are created in the namespace defined in the environment variable <envvar>
    When pods exist
    Then pods in namespace <envvar> with label <k8slabel> are in a <podstate> state

    Examples:
    | buildpath | envvar | k8slabel | podstate |
    | build | KUBE_NAMESPACE | ska-pst=DSP.DISK | Running |


Scenario Outline: kubernetes resources cleanup
    Given manifests exists in <buildpath>
    And kubernetes resources are created in the namespace defined in the environment variable <envvar>
    When kubernetes resources are scheduled for deletion
    Then the namespace present in the env var <envvar> is empty of kubernetes resources

    Examples:
    | buildpath | envvar |
    | build | KUBE_NAMESPACE |
