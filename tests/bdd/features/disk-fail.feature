Feature: DSP.DISK fails gracefully
Scenario Outline: Generate k8s manifest
    Given path <chartpath> contains helm chart <chart>
    And path <overridepath> contains override file <override>
    When helm chart <chart> in <chartpath> is rendered using <override> in <overridepath> to <buildpath>
    Then helm chart <chart> rendered manifests exists in <buildpath>

    Examples:
    | chartpath | chart | overridepath | override | buildpath |
    | charts | ska-pst-dsp | tests/integration/k8s-test | component.bdd.yaml | build |


Scenario Outline: Deploy k8s manifest
    Given manifests exists in <buildpath>
    And environment variable for k8s namespace <envvar> is defined
    When manifests in <buildpath> gets deployed
    Then kubernetes resources are created in the namespace defined in the environment variable <envvar>

    Examples:
    | buildpath | envvar |
    | build | KUBE_NAMESPACE |


Scenario Outline: Deployed k8s pods are in its expected state
    Given manifests exists in <buildpath>
    And kubernetes resources are created in the namespace defined in the environment variable <envvar>
    When pods exist
    Then pods in namespace <envvar> with label <k8slabel> are in a <podstate> state

    Examples:
    | buildpath | envvar | k8slabel | podstate |
    | build | KUBE_NAMESPACE | bdd-test=component-invalid-config | Failed |
    | build | KUBE_NAMESPACE | bdd-test=component-sigint | Succeeded |
    | build | KUBE_NAMESPACE | bdd-test=component-sigterm | Succeeded |


Scenario Outline: Logs of pods in its expected state contains expected component test keywords
    Given pods in the namespace present in the env var <envvar> with label <k8slabel> are in a <podstate> state
    When pods logs are not empty
    Then pod logs must contain the keyword(s) <logkeywords>

    Examples:
    | envvar | k8slabel | podstate | logkeywords |
    | KUBE_NAMESPACE | bdd-test=component-invalid-config | Failed | get_val key [DATA_KEY] did not exist |
    | KUBE_NAMESPACE | bdd-test=component-sigint | Succeeded | received signal 2 |
    | KUBE_NAMESPACE | bdd-test=component-sigterm | Succeeded | received signal 15 |


Scenario Outline: kubernetes resources cleanup
    Given manifests exists in <buildpath>
    And kubernetes resources are created in the namespace defined in the environment variable <envvar>
    When kubernetes resources are scheduled for deletion
    Then the namespace present in the env var <envvar> is empty of kubernetes resources

    Examples:
    | buildpath | envvar |
    | build | KUBE_NAMESPACE |
