# SKA PST DSP
This project provides the C++ library and applications for the DSP Disk component of the Pulsar Timing instrument for SKA Mid and SKA Low.

## Documentation

[![Documentation Status](https://readthedocs.org/projects/ska-telescope-ska-pst-dsp/badge/?version=latest)](https://developer.skao.int/projects/ska-pst-dsp/en/latest/)

The documentation for this project, including the package description, Architecture description and the API modules can be found at SKA developer portal:  [https://developer.skao.int/projects/ska-pst-dsp/en/latest/](https://developer.skao.int/projects/ska-pst-dsp/en/latest/)

## Build Instructions

Firstly clone this repo and submodules to your local file system

    git clone --recursive git@gitlab.com:ska-telescope/pst/ska-pst-dsp.git

then change to the newly cloned directory and create the build/ sub-directory

    cd ska-pst-dsp
    mkdir build

To simulate the build that will be performed in the Gitlab CI, a C++ builder image that has been extended from the ska-cicd-cpp-build-base [C++ building image](https://github.com/ska-telescope/cpp_build_base) is used. This image includes the required OS package and other custom software dependencies. The current version of this image is defined in .gitlab-ci.yml; e.g.

    grep SKA_CPP_DOCKER_BUILDER_IMAGE .gitlab-ci.yml

After verifying the current builder image version, pull it with a command like the following

    docker pull registry.gitlab.com/ska-telescope/pst/ska-pst-smrb/ska-pst-smrb-builder:0.10.6

Now launch this builder image as a container. Note the current working directory will be mounted into the container as /mnt/ska-pst-dsp.

    make local-dev-env

The library and applications can be built using the standardise makefile templates provided by CICD infrastructure.

### Debug Build

The debug build will use the Cmake build arguments `-DCMAKE_BUILD_TYPE=Debug -DCMAKE_CXX_FLAGS="-coverage" -DCMAKE_EXE_LINKER_FLAGS="-coverage"`. For debug purposes, the `#define DEBUG` will be defined for the software to enable any debug features.

    make local-cpp-build-debug

### Release Build

The release build will use the Cmake build arguments `-DCMAKE_BUILD_TYPE=Release` which ensures `#define DEBUG` is not defined. This build should be used for all deployments.

    make local-cpp-build-release

### Linting Build

This build target compiles the library and applications with the flags required for linting and static analysis.

    make local-cpp-ci-simulation-lint

During the compilation, the build generates `compile_commands.json` file which is used in the linting and static analysis tools: clang-tidy, cppcheck and IWYU.

### Documentation Build

API documentation for the library is genereated with Doxygen, which is then converted into ReadTheDocs format by Sphinx, Breathe and Exhale. The documentation is built via 

    make docs-build html

## Building Docker Image

To build an OCI Docker image for testing the applications:

    make OCI_IMAGE_BUILD_CONTEXT=${PWD} oci-build-all

## Tests

The unit tests for the library can be executed via the make target:

    make local-cpp-ci-simulation-test

This will compile the software using the [Debug Build](#debug-build) and then execute test and test_memcheck make targets

## Gitlab CI Simulation

To simulate the end to end build and test related jobs performed the gitlab runner

    make local-cpp-ci-simulation

To simulate just the linting part of the simulation use:

    make local-cpp-ci-simulation-lint

To simulate just testing use:

    make local-cpp-ci-simulation-test

To generate code coverage reports, use:

    make local-cpp-ci-simulation-coverage

## Local Execution

## Helm Charts

A Helm chart consists of [template files](./charts/ska-pst-dsp/templates) that are rendered based on the contents of a payload, i.e. [values.yaml](./charts/ska-pst-dsp/values.yaml)

### Helpers

### Manual k8s deployment

### Simulating component tests

## Known bugs

## Troubleshooting

## License

See the LICENSE file for details.
