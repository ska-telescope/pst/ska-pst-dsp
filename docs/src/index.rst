SKA-PST-DSP
=============

This project provides the libraries and applications required for the DSP
software component of the Square Kilometre Array (SKA)'s Pulsar Timing (PST) instrument. 

.. README =============================================================

.. toctree::
  :maxdepth: 1
  :caption: Readme
  :hidden:

  ../../README.md

.. COMMUNITY SECTION ==================================================

..

.. toctree::
  :maxdepth: 2
  :caption: Architecture
  :hidden:

  architecture/index

.. toctree::
  :maxdepth: 2
  :caption: Applications
  :hidden:

  apps/index

.. toctree::
  :maxdepth: 3
  :caption: API
  :hidden:

  api/library_root

.. Applications
.. ==================================

.. - :doc:`apps/index`

.. API
.. ==================================

.. These are all the packages, functions and scripts that form part of the project.

.. - :doc:`api/library_root`
